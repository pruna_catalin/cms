<div id="page-content">
    <section id="car-advertisement">
        <div class="content-holder">
            <div class="slide-part-large">
                <div class="visible-image">
                    <h2><span class="bold"><?php echo Lang::message_front("BestOffer"); ?></span></h2>
                    <div id="slides">
                        <ul class="slides">
                            <?php
                            $content = "";
                            foreach ($images_front as $image) {
                                $content .= '<li>'
                                        . '<a href="?product=' . $recent_products[0]->id . '">'
                                        . '<img src="' . App::BasePath() . '/images/' . $image->name . '" alt="Slide" />'
                                        . '<div class="slide-info">
                                                                                            <h3>' . $recent_products[0]->name_model . ' ' . $recent_products[0]->name . '</h3>
                                                                                            <span class="price-tag">' . $recent_products[0]->price . ' ' . $recent_products[0]->currency . '</span>								
                                                                                    </div>
                                                                              </a>
                                                                    </li>';
                            }
                            echo $content;
                            ?>

                        </ul>
                    </div>
                </div>
            </div>

            <div class="latest-offers">
                <div class="headline">
                    <h2><span class="bold"><?php echo Lang::message_front("LatestOffers"); ?></span></h2>
                    <a href="#" class="scroll-up scroll-icon"><span><?php echo Lang::message_front("Up"); ?></span></a>
                    <a href="#" class="scroll-down scroll-icon"><span><?php echo Lang::message_front("Down"); ?></span></a>
                </div>

                <ul class="offer-list offer-small list-content">
                    <?php
                            $content = "";
                            foreach ($recent_products as $product) {
                                
                                $content .= '<li>'
                                        . '<a href="?product=' . $product->id . '">'
                                        . '<img src="' . App::BasePath() . '/images/' . $image->name . '" alt="Slide" />'
                                        . '<div class="entry-label">
                                                                                            <h4>' . $product->name_model . ' ' . $product->name . '</h4>
                                                                                            <span class="price-tag">' . $product->price . ' ' . $product->currency . '</span>								
                                                                                    </div>
                                                                              </a>
                                                                    </li>';
                            }
                            echo $content;
                            ?>

                  
                </ul>

                <div class="dealer-tooltip">
                    <div><a href="#"></a></div>
                </div>

            </div><!--.latest-offers-->
        </div>

    </section><!--#car-advertisement-->

    <section id="car-shortcuts">
        <div class="content-holder">

        

            <div class="full-width articles-dealers-offers">
               

                <div class="daily-offers one-third sideRight">
                    <div id="changeContent"></div>
                    <h3><span class="bold"><?php echo Lang::message_front("SubScribe"); ?></span></h3>
                    <form action="javascript:void(0);" id="formProduct">
                        <input  type="text" placeholder="<?php echo Lang::message_front("EmailHere"); ?>" class="email-address default-input " name="SubScribe[email]">
                        <p class="offer-text"> </p>
                        <div class="submit-button"><input type="submit" value="Subscribe" onclick="subscribe();" /></div>
                        <p class="amount-subscribers">+ <?php echo $countUsers; ?>  </p>
                    </form>
                </div><!--.daily-offers-->
            </div>
        </div>
    </section><!--#car-shortcuts-->		

</div><!--#page-content-->