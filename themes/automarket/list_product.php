<div id="page-content">

    <section id="car-pagination">
        <div class="content-holder">
            <div class="view-select-tabs">
                <a href="#" id="list-view"><span>List View</span></a>
                <a href="#" id="grid-view" class="current"><span>Grid View</span></a>
            </div>
        </div>
    </section><!--#car-pagination -->

    <section id="search-list">
        <div class="content-holder">
            <div class="full-width">

                <div class="one-half col-241 search-area">
                 
                </div>

                <div class="results-list one-half col-701">
                    <div class="sort-view layer-one">

                        <div id="sort-by">
                            <select name="sort-by">
                                <option selected="selected">Sort by</option>
                                <option>Price ASC</option>
                                <option>Price DESC</option>
                            </select>
                        </div>

                    </div>

                    <div class="layer-two">

                        <div id="cars-list" class="grid-view list-content">
                            <ul class="offer-small">


                                <!-- Car post -->
                               <?php echo $list; ?>

                            </ul>					
                        </div>

                    </div><!--.layer-two-->

                  
                </div>


            </div>
        </div>
    </section><!--#search-list-->

</div><!--#page-content-->