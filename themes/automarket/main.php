<?php 
$newApp = new App();
?>
<!DOCTYPE html>
<html class="no-js">
	<head>
		<meta charset="utf-8">
		<title>Deposit Store - Home</title>
		<meta name="description" content="Automarket">

		<link rel="stylesheet" href="<?php echo App::LivePath();?>/css/main.css">
		<link rel="stylesheet" href="<?php echo App::LivePath();?>/css/uniform.default.css">
		<link rel="stylesheet" href="<?php echo App::LivePath();?>/css/prettyPhoto.css">
		<script src="<?php echo App::LivePath();?>/js/vendor/modernizr-2.6.2.min.js"></script>
		<script src="<?php echo App::LivePath();?>/js/vendor/jquery-1.8.2.min.js"></script>
		<script src="<?php echo App::LivePath();?>/js/vendor/selectivizr.js"></script>
		<script src="<?php echo App::LivePath();?>/js/vendor/PIE.js"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.placeholder.min.js"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.uniform.min.js"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.flexslider-min.js"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.carouFredSel-6.1.0-packed.js"></script>
		<script src="http://maps.googleapis.com/maps/api/js?sensor=false"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.prettyPhoto.js"></script>
		<script src="<?php echo App::LivePath();?>/js/plugins/jquery.countdown.js"></script>
		
		<script src="<?php echo App::LivePath();?>/js/plugins.js"></script>
		<script src="<?php echo App::LivePath();?>/js/main.js"></script>
		<script src="<?php echo App::LivePath();?>/js/custom.js"></script>               
                <script src="<?php echo App::BasePath();?>/admin/js/bootbox.min.js"></script>
                <!-- Latest compiled and minified CSS -->


<!-- Latest compiled and minified JavaScript -->

	</head>
	<body>
		<header>
			<div class="content-holder">
				<div class="layer-one">
					<div class="page-titles">
						<h1><a href="?index"><?php echo AppName; ?></a></h1>
						<p class="sub-title"><?php echo AppTitle; ?></p>
					</div>
						
					<div class="dealer-login">
						<a href="?case=account" class="dealer-name">
                                                 <?php echo  (isset($_SESSION['login']))?$username : Lang::message_front("Anonymous"); ?></a>
                                          <div class="header-buttons">
						<a href="?case=shop" class="add-an-offer rounded-link-box" ><span class="box-content"><strong class="plus-sign">+</strong>&nbsp;<?php echo Lang::message_front("Shop"); ?></span></a>      
                                          </div>
					</div>
						
					<div class="header-buttons">
                                           
						
						</div>
					</div>
				</div><!--.layer-one-->
					
				<div class="layer-two">
					<nav>
						<ul>
							<li class="components current-item">
                                                            <img src="<?php echo App::LivePath();?>/images/pc.png" class="img_menu"/>
                                                            <a href="?case=pc"><?php echo Lang::message_front("PC"); ?></a></li>
							<li class="components current-item"><a href="?case=laptop">
                                                                <img src="<?php echo App::LivePath();?>/images/laptop.png" class="img_menu"/><br>
                                                            <?php echo Lang::message_front("Laptops"); ?></a></li>
							<li class="components current-item"><a href="?case=monitor">
                                                             <img src="<?php echo App::LivePath();?>/images/monitor.png" class="img_menu"/>    
                                                            <?php echo Lang::message_front("Monitors"); ?></a></li>
							<li class="components current-item"><a href="?case=component">
                                                             <img src="<?php echo App::LivePath();?>/images/components.png" class="img_menu"/>
                                                            <?php echo Lang::message_front("Components"); ?></a></li>
						</ul>
					</nav>
						
					<form id="header-search" action="?case=search" method="post">
		            	<input type="text" name="quick_search" onfocus="if(this.value == '<?php echo Lang::message_front("Search"); ?>') { this.value = ''; }" onblur="if(this.value == '') { this.value = '<?php echo Lang::message_front("Search"); ?>'; }" placeholder="<?php echo Lang::message_front("Search"); ?>" class="quick-search" >
						
						<div class="search-submit">
							<input type="submit" value="Search"/>
						</div>
					</form>
				</div><!--.layer-two-->
			</div>
		</header>
		<?php echo $params; ?>
	<footer>
		
			
		<div class="layer-two">
			<div class="content-holder">
				<p class="copyright">&copy; <?php echo date("Y",time()). " ". AppName; ?></p>
				
			</div>
		</div><!--.layer-two-->
</footer>
	</body>
</html>
