<?php

/*
 * Description of File
 * 
 * IM
 */
namespace DubInfo_gestion_immobilier\model;

use DubInfo_gestion_immobilier\model\exceptions\StringAttributeTooLong;
use DubInfo_gestion_immobilier\model\exceptions\BadTypeException;

class File implements \JsonSerializable{
	 const MAX_SIZE_NAME= 255;
	 const MAX_SIZE_FILENAME= 75;
	 const MAX_SIZE_EXT= 5;

	/**
     *
     * @var int 
     */
    private $_id;
	
	/**
     *
     * @var int 
     */
    private $_size;
	
	/**
     *
     * @var string 
     */
    private $_name;
	
	/**
     *
     * @var string 
     */
    private $_filename;
	
	/**
     *
     * @var string 
     */
    private $_type;
	
	/**
     *
     * @var string 
     */
	private $_temp;
	
	public function __construct($id = NULL, $name = NULL, $filename = NULL, $temp = NULL) {
		$this->setId($id);
		$this->setName($name);
		
		$this->setFileName($filename);
		
		$fileext = pathinfo($filename, PATHINFO_EXTENSION);
		$this->setType($fileext);
		
		if($fileext === 'jpeg' || $fileext === 'jpg' || 
				$fileext === 'png' || $fileext === 'gif')
			$file_path = HOUSE_IMAGES_PATH.$filename;
		else
			$file_path = HOUSE_DOCUMENTS_PATH.$filename;
		
		if(file_exists($file_path)) {
			$file_size = filesize($file_path);
			$this->setSize($file_size);
		}
		
		$this->setTemporary($temp);
    }
	
//<editor-fold defaultstate="collapsed" desc="Id">
    /**
     * 
     * @return int
     */
    public function getId() {
        return $this->_id;
    }
    
    /**
     * 
     * @param int $id
     * @throws BadTypeException
     */
    public function setId($id) {
        $this->_id = CheckTyper::isInteger($id, 'id', __CLASS__);
    }
//</editor-fold>
	
//<editor-fold defaultstate="collapsed" desc="Name">
	/**
     * 
     * @return string
     */
    public function getName() {
        return $this->_name;
    }
    
    /**
     * 
     * @param string $name
     * @throws BadTypeException
     * @throws StringAttributeTooLong
     */
    public function setName($name) {
        $_name = CheckTyper::isString($name, 'name', __CLASS__);
        
        if(strlen($_name) > self::MAX_SIZE_NAME) {
            throw new StringAttributeTooLong('name', __CLASS__);
        }
        
        $this->_name = $_name;
    }
//</editor-fold>	
	
//<editor-fold defaultstate="collapsed" desc="FileName">
	/**
     * 
     * @return string
     */
    public function getFileName() {
        return $this->_filename;
    }
    
    /**
     * 
     * @param string $filename
     * @throws BadTypeException
     * @throws StringAttributeTooLong
     */
    public function setFileName($filename) {
        $_filename = CheckTyper::isString($filename, 'filename', __CLASS__);
    
        if(strlen($_filename) > self::MAX_SIZE_FILENAME) {
            throw new StringAttributeTooLong('filename', __CLASS__);
        }
        
        $this->_filename = $_filename;
    }
//</editor-fold>

//<editor-fold defaultstate="collapsed" desc="Type">
	/**
     * 
     * @return string
     */
    public function getType() {
        return $this->_type;
    }
    
    /**
     * 
     * @param string $type
     * @throws BadTypeException
     * @throws StringAttributeTooLong
     */
    public function setType($type) {
        $_type = CheckTyper::isString($type, 'type', __CLASS__);
        
        if(strlen($_type) > self::MAX_SIZE_EXT) {
            throw new StringAttributeTooLong('type', __CLASS__);
        }
        
        $this->_type = $_type;
    }
//</editor-fold>
	
//<editor-fold defaultstate="collapsed" desc="Temporary">
	/**
     * 
     * @return string
     */
    public function getTemporary() {
        return $this->_temp;
    }
    
    /**
     * 
     * @param string $temp
     * @throws BadTypeException
     * @throws StringAttributeTooLong
     */
    public function setTemporary($temp) {
        $_temp = CheckTyper::isString($temp, 'temporary', __CLASS__);
        
        if(strlen($_temp) > self::MAX_SIZE_EXT) {
            throw new StringAttributeTooLong('temporary', __CLASS__);
        }
        
        $this->_temp = $_temp;
    }
//</editor-fold>
	
//<editor-fold defaultstate="collapsed" desc="Size">
    /**
     * 
     * @return int
     */
    public function getSize() {
        return $this->_size;
    }
    
    /**
     * 
     * @param int $size
     * @throws BadTypeException
     */
    public function setSize($size) {
        $this->_size = CheckTyper::isInteger($size, 'size', __CLASS__);
    }
//</editor-fold>
	
	/*
	 * Efface les fichiers des répertoires correspondants 
	 * If IMAGE -> delete from HOUSE_IMAGES_PATH
	 * Else If DOC -> delete from HOUSE_DOCUMENTS_PATH
	 * Else TEMP -> delete from HOUSE_TMPFILES_PATH
	 * 
	 */
	public function delete() {
		if($this->getTemporary() === 'no') {
			if($this->getType() === 'jpeg' || $this->getType() === 'jpg' || 
					$this->getType() === 'png' || $this->getType() === 'gif') {
				$file_path = HOUSE_IMAGES_PATH.$this->getFileName();
				if(file_exists($file_path)) {
					unlink($file_path);
					$file_thumb_path = HOUSE_THUMBNAILS_PATH.$this->getFileName();
					if(file_exists($file_thumb_path))
						unlink($file_thumb_path);
				}
			}else {
				$file_path = HOUSE_DOCUMENTS_PATH.$this->getFileName();
				if(file_exists($file_path))
					unlink($file_path);
			}
		}else {
			$file_path = HOUSE_TMPFILES_PATH.$this->getFileName();
			if(file_exists($file_path))
				unlink($file_path);
		}
	}
	
	public function jsonSerialize() {
        return [
			'name' => $this->getName(),
			'filename' => $this->getFileName(),
			'size' => $this->getSize(),
			'type' => $this->getType()
		];
    }
}
	