<?php
define("ORDER" , "ORDER BY");
define("LIMIT" , "LIMIT");
define("SELECT" , "SELECT");
define("CONDITION" ,"WHERE");
define("UPDATE" ,"UPDATE");
define("DELETE" ,"DELETE");
define("INSERT" , "INSERT INTO");
require_once("includes/config.php");
class AppSql{	
	/**
		Search  by diferent method and condition 
	**/
	public static function findAll($options = "*",$table="",  $params = [], $limit = "0 , 25", $orderType = "ASC",$orderBy = ""){
		global $connexion;
		$result = "";
		$condition = "";
		$params_map = AppSql::MapParamasQuery($params,"SELECT");
		
		if($table != ""){
			if(sizeof($params) > 0){
				$condition .= CONDITION." ".$params_map[0];
			}else{
				$condition = "";
			}
			if($orderBy != ""){
				$orderBy = " ".ORDER." ".$orderBy." ".$orderType;
			}
                        if(strlen($limit) > 0){
                            $condition .= $orderBy." LIMIT ".$limit;
                        }else{
                            $condition .= $orderBy;
                        }
			
			$sql = SELECT." ".$options."  FROM ".$table." ".$condition."";
				if(sizeof($params) > 0 ){
					$result = $connexion->prepare($sql);
					$result->execute($params);
					$result = $result->fetchAll(PDO::FETCH_OBJ);
				}else{
					$result = $connexion->prepare($sql);
					$result->execute();
					$result = $result->fetchAll(PDO::FETCH_OBJ);
				}
		}
		return $result;
	}
	/**
		Update  item 
	**/
	public static function update($table="",$condition = [],$params = []){
		global $connexion;
		$result = "";
		$condition_val = "";
		$params_map = AppSql::MapParamasQuery($params,"UPDATE");
		$condition_map = AppSql::MapParamasQuery($condition,"SELECT");
		if($table != ""){
			if(sizeof($params) > 0){
				$condition_val .= CONDITION." ".$condition_map[0];
			}else{
				$condition_val = "";
			}
			$sql = UPDATE." ".$table."  SET ".$params_map[0]." ".$condition_val."";
			foreach ($condition as $key => $value) {
				$params[$key] = $value;
			}
			if(sizeof($params) > 0 ){
				$result = $connexion->prepare($sql);
				$result->execute($params);
			}
		}
		return $result;
	}
	/**
		Delete  item from tables 
	**/
	public static function delete($table="",$condition = []){
		global $connexion;
		$result = "";
		$condition_map = "";
		$params_map = AppSql::MapParamasQuery($condition,"DELETE");
		if($table != ""){
			$condition_map .= CONDITION." ".$params_map[0];
			$sql = DELETE." FROM ".$table." ".$condition_map."";
			$result = $connexion->prepare($sql);
			$result->execute($condition);
		}
		return $result;
	}
	/**
		Insert New item in tables 
	**/
	public static function insert($table = "" , $params = []){
		global $connexion;
		$result = "";
		$condition = "";
		$params_map = AppSql::MapParamasQuery($params,"INSERT");
		if($table != ""){
			
		$columns = implode(",",$params_map[2]);
		$sql = INSERT." ".$table." (".$columns.") VALUES (".$params_map[0].")";
                echo $sql;
			if(sizeof($params) > 0 ){
				$result = $connexion->prepare($sql);
				$result->execute($params);
				$result = $connexion->lastInsertId();
			}
		}
		return $result;
	}
	/**
		Call Stocked Procedure
	**/
	
	public static function callProcedure($name = "" , $params = []){
		// $stmt = $dbh->prepare("CALL sp_returns_string(?)");
		// $stmt->bindParam(1, $return_value, PDO::PARAM_STR, 4000); 

		// // call the stored procedure
		// $stmt->execute();

		// print "procedure returned $return_value\n";
	}
	/**
		Return Map of params for sql sintax
		
	**/
	private static function MapParamasQuery($array_obj,$type){
		$value = [];
		$columns = "";
		$result = [];
		$keys =  [];
		$i= 1;
		$size = sizeof($array_obj);
		foreach($array_obj as $key => $obj){
			$key = str_replace(":","",$key);
			if($i == $size){
				if($type == "SELECT"){
					$columns .= "`".$key."`=:".$key;
				}else if($type == "INSERT"){
					$columns .= ":".$key;
				}else if($type == "UPDATE"){
					$columns .= "`".$key."`=:".$key;
				}else if($type == "DELETE"){
					$columns .= "`".$key."`=:".$key;
				}
			}else{
				if($type == "SELECT"){
					$columns .= "`".$key."`=:".$key." AND ";	
				}else if($type == "INSERT"){
					$columns .= ":".$key." , ";	
				}else if($type == "UPDATE"){
					$columns .= "`".$key."`=:".$key." , ";	
				}else if($type == "DELETE"){
					$columns .= "`".$key."`=:".$key." , ";	
				}									
			}
			array_push($value, $obj);
			array_push($keys,$key);
			$i++;
		}
		array_push($result,$columns);
		array_push($result,$value);
		array_push($result,$keys);
		return $result;
	}
         public static function Count($options = "*",$table="",  $params = []){
            global $connexion;
		$result = "";
		$condition = "";
		$params_map = AppSql::MapParamasQuery($params,"SELECT");
		if($table != ""){
			if(sizeof($params) > 0){
				$condition .= CONDITION." ".$params_map[0];
			}else{
				$condition = "";
			}
			
			$sql = SELECT." Count(*) ".$options."  FROM ".$table." ".$condition."";
				if(sizeof($params) > 0 ){
					$result = $connexion->prepare($sql);
					$result->execute($params);
					$result = $result->fetch();
				}else{
					$result = $connexion->prepare($sql);
					$result->execute();
					$result = $result->fetch();
				}
		}
		return $result;
        }
	private static function bind($param, $value, $type = null){
		global $connexion;
	    if (is_null($type)) {
		        switch (true) {
		            case is_int($value):
		                $type = PDO::PARAM_INT;
		                break;
		            case is_bool($value):
		                $type = PDO::PARAM_BOOL;
		                break;
		            case is_null($value):
		                $type = PDO::PARAM_NULL;
		                break;
		            default:
		                $type = PDO::PARAM_STR;
		        }
		    }
		$this->connexion->bindValue($param, $value, $type);
	}
}


?>