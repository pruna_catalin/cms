<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Load
 *
 * @author SelfMind
 */
class Load {
    public static function LoadFunctions(){
        $params = "";
        if(isset($_GET['case'])){
            switch ($_GET['case']){
                    case "account":
                        $params = SiteController::actionAccount();
                        break;
                    case "laptop":
                        $pagination = (isset($_POST['pag']) && is_numeric($_POST['pag']))?$_POST['pag']: 25;
                        $params = SiteController::actionListProduct("laptop",$pagination);
                        break;
                    case "pc":
                        $pagination = (isset($_POST['pag']) && is_numeric($_POST['pag']))?$_POST['pag']: 25;
                        $params = SiteController::actionListProduct("pc",$pagination);
                        break;
                    case "monitor":
                        $pagination = (isset($_POST['pag']) && is_numeric($_POST['pag']))?$_POST['pag']: 25;
                        $params = SiteController::actionListProduct("monitor",$pagination);
                        break;
                    case "component":
                        $pagination = (isset($_POST['pag']) && is_numeric($_POST['pag']))?$_POST['pag']: 25;
                        $params = SiteController::actionListProduct("component",$pagination);
                        break;
                    case "subscribe":
                        $params = SiteController::actionSubscribe($_POST);
                        break;
                   
                    default :
                        $params = SiteController::actionProduct();
                        break;
                }    
        }else if(isset($_GET['product']) && is_numeric($_GET['product'])){
          
                $params = SiteController::actionProduct();
            
        }else{
            $params = SiteController::actionProduct();
        }
                
                
                $view = new View("main",array("params"=>$params));
                echo $view->render();
    }
   public static function CreateApp(){
            
            if(!DEVELOPER_MODE){

            }else{
                Login::Request();
                Load::LoadFunctions();
            }
    }
}
