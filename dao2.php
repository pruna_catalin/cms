<?php
namespace DubInfo_gestion_immobilier\controller\data;

use DubInfo_gestion_immobilier\model\File;
use DubInfo_gestion_immobilier\model\exceptions\PDOException;

class DAOFileUpload extends AbstractDAO{
	public function add($upFile) {
		try {
            $sql = "INSERT INTO attachments (filename, filepath, maison_id, etat_maison_id) 
					VALUES (:filename, :filepath, :maison_id, :etat_maison_id)";
            $request = $this->getConnection()->prepare($sql);
			foreach($upFile->getFiles() as $file) {
				$this->execute($request, 
					array(
						':filename' => $file->getName(),
						':filepath' => $file->getFileName(),
						':maison_id' => $upFile->getIdMaison(),
						':etat_maison_id' => $upFile->getEtat()->getId()
					)
				);
			}
			return [];
        } catch (Exception $ex) {
            throw new PDOException($ex->getMessage());
        }
	}

	public function delete($upFile) {
		try {
			$sql = "DELETE FROM attachments WHERE filepath=:filepath AND maison_id=:maison_id AND etat_maison_id=:etat_maison_id";
			$request = $this->getConnection()->prepare($sql);

			foreach($upFile->getFiles() as $file) {
				$this->execute($request, 
					array(
						':filepath' => $file->getFileName(),
						':maison_id' => $upFile->getIdMaison(),
						':etat_maison_id' => $upFile->getEtat()->getId()
					)
				);

				$file->delete();
			}
			return [];
        } catch (Exception $ex) {
            throw new PDOException($ex->getMessage());
        }
	}

	public function read($id) {
		return ['erreur' => 'Read pas implémentée'];
	}

	public function readAll() {
		return ['erreur' => 'ReadAll pas implémentée'];
	}

	public function readList($upFile = NULL) {
		if($upFile == NULL)
			return [];
		
		try{
            $sql = "SELECT * FROM attachments 
					WHERE maison_id=:maison_id AND etat_maison_id=:etat_maison_id ORDER BY id";
            $request = $this->getConnection()->prepare($sql);
            $this->execute($request, 
				array(
					':maison_id' => $upFile->getIdMaison(),
					':etat_maison_id' => $upFile->getEtat()->getId()
				)
			);
            
			
            foreach ($request->fetchAll(\PDO::FETCH_ASSOC) as $result) {
                $files[] = new File($result['id'], $result['filename'], $result['filepath'], 'no');
            }
			
            return isset($files) ?  $files : [];
            
        } catch (Exception $ex) {
            throw new PDOException($ex->getMessage());
        }
	}

	public function update($object) {
		return ['erreur' => 'Update pas implémentée'];
	}

}