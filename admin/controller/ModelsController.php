<?php

class ModelsController{
    public static function ListModelsSelect(){
       return  AppSql::findAll("*","`prd_model`",array(":enable" => "1"),"0,100","DESC");
    }
    public static function ListModels($params = []){
          $grid_models = "";
          $count = 0;
            if(sizeOf($params) > 0){
                $models_list = AppSql::findAll("*","`prd_model`",array(":enable" => "1"),$params[0].",".$params[1],"DESC");
            }else{
               $models_list = AppSql::findAll("*","`prd_model`",array(":enable" => "1"),"0,5","DESC");
            } 
            
           foreach($models_list as $item){
                $grid_models .= "<tr class='gradeX'><td>".$item->name."</td><td>".$item->nr."</td><td>
                    <a href='#accSettings1' role='button' class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='' onclick='editModel(".$item->id.");'> Edit
                    </a>
                    <a href='#' class='btn btn-danger btn-small hidden-phone' data-original-title='' onclick='delModel(".$item->id.");'>Delete</a>
                    </td></tr>";
                    $count++;
            }
          $view = new View("models",array("gridList" => $grid_models,"count"=> $count));
          return $view->render();
    }
    public static function findByIdTable($id){
          $model_list = AppSql::findAll("*","`prd_model` WHERE id = ".$id."",array(),"0,25","DESC");
         
         return $model_list;
     }
     public static function findById($id){
          $model_list = AppSql::findAll("*","`prd_model` WHERE id = ".$id."",array(),"0,25","DESC");
         
          echo  json_encode(array("success"=>true,"content"=> $model_list));
     }
     public static function update($data){
          $result = 0;
          $models = $data['Models'];
          if(strlen($models['name']) == 0 ){
              unset($models['name']);
          }
          if(strlen($models['nr']) == 0 ){
              unset($models['nr']);
          }
          AppSql::update("`prd_model`",array("id"=>$models['id']), $models);
        echo json_encode(array("success"=>true,"content"=>ModelsController::GridAjax(array())));
      }
      public static function delete($data){
          $result = 0;
          $models = [];
          $models['id'] = $data;
          $models['enable'] = 0;
        
          AppSql::update("`prd_model`",array("id"=>$models['id']), $models);
        echo json_encode(array("success"=>true,"content"=>ModelsController::GridAjax(array())));
      }
     public static function GridAjax($params = []){
        $grid_models = "";
        $count = 0;
        $models_list = AppSql::findAll("*","`prd_model`",array("enable"=> "1"),"0,5","DESC");
        foreach($models_list as $item){
            $grid_models .= "<tr class='gradeX'><td>".$item->name."</td><td>".$item->nr."</td><td>
                <a href='#accSettings1' role='button' class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='' onclick='editModel(".$item->id.");'> Edit
                </a>
                <a href='#' class='btn btn-danger btn-small hidden-phone' data-original-title='' onclick='delModel(".$item->id.");'>Delete</a>
                </td></tr>";
                $count++;
       }
        return $grid_models;
    }
     public static function newModel($data){
          $result = 0;
          $error = "";
          $models = $data['Models'];
          $models['enable'] = 1;
          $newUser =   AppSql::insert("`prd_model`",$models);
          echo json_encode(array("success"=>true,"content"=>ModelsController::GridAjax(array()),"error"=>$error));

     }
}


?>