<?php

include "ControllerLoad.php";

class AdminController {

    public $content = "";
   
    public static function Init() {
        ThemeBase::Load();
        if (LoginController::CheckLogin() == true) {
            $case = (isset($_GET['case'])) ? $_GET['case'] : "index";
            switch ($case) {
                case 'index':
                    $content = DashBoardController::Index("",1);
                    AdminController::Index($content, 1);
                    break;
                case 'users':
                    if (isset($_GET['findById'])) {
                        $id = (is_numeric($_GET['findById']) ? $_GET['findById'] : 0);
                        UsersController::findById($id);
                    } else if (isset($_GET['update'])) {
                        UsersController::update($_POST);
                    } else if (isset($_GET['new'])) {
                        UsersController::newUser($_POST);
                    } else if (isset($_GET['delete'])) {
                        $id = (is_numeric($_GET['delete']) ? $_GET['delete'] : 0);
                        UsersController::delete($id);
                    } else {
                        if (isset($_GET['pagination'])) {
                            $content = UsersController::Grid(array((is_numeric($_GET['pagination'])) ? $_GET['pagination'] : 0, 5));
                            AdminController::Index($content, 4);
                        } else {
                            $content = UsersController::Grid(array());
                            AdminController::Index($content, 4);
                        }
                    }
                    break;
                case 'settings':
                    $content = SettingsController::PanelSettings();
                    AdminController::Index($content, 13);
                    break;
                case 'products':
                    if (isset($_GET['findById'])) {
                        $id = (is_numeric($_GET['findById']) ? $_GET['findById'] : 0);
                        ProductController::findById($id);
                    } else if (isset($_GET['update'])) {
                        ProductController::update($_POST);
                    } else if (isset($_GET['delete'])) {
                        $id = (is_numeric($_GET['delete']) ? $_GET['delete'] : 0);
                        $del_product = ProductController::delete($id);
                        $content = ProductController::ListProducts(array(), $del_product);
                        AdminController::Index($content, 5);
                    } else if (isset($_GET['new'])) {
                        ProductController::newProduct($_POST);
                    } else if (isset($_GET['pagination'])) {
                        $content = ProductController::ListProducts(array((is_numeric($_GET['pagination'])) ? $_GET['pagination'] : 0, 5));
                        AdminController::Index($content, 5);
                    } else if (isset($_POST['Product'])) {
                        $add_product = ProductController::AddProduct($_POST);
                        $content = ProductController::ListProducts(array(), $add_product);
                        AdminController::Index($content, 5);
                    }else if (isset($_GET['upload'])) {
                        $uploadImg = new ProductController();
                        $add_image = $uploadImg->uploadImages($_FILES);
                        //AdminController::Index($content, 5);
                    }else if (isset($_GET['pdf'])) {
                        ProductController::Pdf();
                        //AdminController::Index($content, 5);
                    } else {
                        $content = ProductController::ListProducts(array(), 0);
                        AdminController::Index($content, 5);
                    }
                    break;
                case 'models':
                    if (isset($_GET['findById'])) {
                        $id = (is_numeric($_GET['findById']) ? $_GET['findById'] : 0);
                        ModelsController::findById($id);
                    } else if (isset($_GET['update'])) {
                        ModelsController::update($_POST);
                    } else if (isset($_GET['delete'])) {
                        $id = (is_numeric($_GET['delete']) ? $_GET['delete'] : 0);
                        ModelsController::delete($id);
                    } else if (isset($_GET['new'])) {
                        ModelsController::newModel($_POST);
                    } else if (isset($_GET['pagination'])) {
                        $content = ModelsController::ListModels(array((is_numeric($_GET['pagination'])) ? $_GET['pagination'] : 0, 5));
                        AdminController::Index($content, 6);
                    } else {
                        $content = ModelsController::ListModels(array());
                        AdminController::Index($content, 6);
                    }
                    break;
                case 'charts':
                    CustomChartsController::DashBoardOrders();
                    break;
                case 'logout':
                    LoginController::Logout();
                default:
                    break;
            }
        } else {
            if (isset($_POST['username']) && isset($_POST['password'])) {
                AdminController::Login();
            }
        }
    }

    public static function Index($content, $item) {
        $selectMenu = $item;
        $view = new View("index", array("content" => $content, "selectMenu" => $selectMenu));
        echo $view->render();
    }

    public static function Login() {
        $username = (isset($_POST['username']) ? $_POST['username'] : "");
        $password = (isset($_POST['password']) ? $_POST['password'] : "");
        $errors = LoginController::Login($username, $password);
		$view = new View("login", array("errors" =>$errors));
        echo $view->render();
    }

}

?>