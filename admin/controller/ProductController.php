<?php

class ProductController {
    
    
    public static function ListProducts($params = [], $state) {
        $grid_products = "";
        $optionsModel = "";
        $coins = "";
        $listModel = ModelsController::ListModelsSelect(array());
        $currency = CurrencyController::getCoins();
		
        foreach ($listModel as $item) {
            $optionsModel .= "<option value='" . $item->id . "'>" . $item->name . "</option>";
        }
		print_R($currency);
			 foreach ($currency->Currency as $key => $item) {
				$coins .= "<option value='" . $item . "'>" . $item . "</option>";
			}
		
       
        $count = 0;
        if (sizeOf($params) > 0) {
            $models_list = AppSql::findAll("*", "`prd_product`", array(":enable" => "1"), $params[0] . "," . $params[1], "DESC");
        } else {
            $models_list = AppSql::findAll("*", "`prd_product`", array(":enable" => "1"), "0,5", "DESC");
        }

        foreach ($models_list as $item) {
            $model = ModelsController::findByIdTable($item->id_model);
            $grid_products .= "<tr class='gradeX'><td>" . $model[0]->name . "</td><td>" . $item->name . "</td><td>" . $item->quantity . "</td><td>" . $item->price . "</td><td>
                    <a href='?case=products&edit=" . $item->id . "' role='button' class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title=''> Edit
                    </a>
                    <a href='?case=products&delete=" . $item->id . "' class='btn btn-danger btn-small hidden-phone' data-original-title='' >Delete</a>
                    </td></tr>";
            $count++;
        }
        $view = new View("product", array("gridList" => $grid_products, 
											"count" => $count, 
											"optionsModel" => $optionsModel,
											"currency" => $coins, 
											"state" => $state));
        return $view->render();
    }

    public static function AddProduct($items = array()) {
        $newProduct = 0;
        if (sizeOf($items) > 0) {
            $result = 0;
            $error = "";
            $models = $items['Product'];
            $models['enable'] = 1;
            $newProduct = AppSql::insert("`prd_product`", $models);
           
        }
        if(isset($_POST['html5_uploader_count'])){
            $models = array();
            $models['id_product'] = $newProduct;
            for($i=0;$i < $_POST['html5_uploader_count'];$i++){
                $models['name'] = $_POST['html5_uploader_'.$i.'_tmpname'];
                $newProduct = AppSql::insert("`prd_image`", $models);
            } 
        }
       
        return "Success Add";
    }

    public static function delete($id) {
        $result = 0;
        $models = [];
        $models['id'] = $id;
        $models['enable'] = 0;

        $delProduct = AppSql::update("`prd_product`", array("id" => $models['id']), $models);
        return "Success Delete";
    }
    
      function uploadImages() {     
        $file_name = $_POST['name'];
        move_uploaded_file($_FILES['file']['tmp_name'], "../images/".$file_name);
    }
     public static function Pdf(){
        
        $products = AppSql::findAll("*", "`prd_product`", array(":enable" => "1"), "0,5", "DESC");
        if(!empty($products)){
            $filename = rand(1000,9999);
            $filepath = './images/pdf/ListaProduse_Nr_'.$filename.'.pdf';
           
            $pdf = new myPDF();
            $pdf->colectData("",$filename,ConditionDelivery,DeliveryTime,Observation,PaymentMode,date("y-m-d",time()),"1000");
            header('Content-type: application/pdf');
            header('Content-Disposition: inline; filename="' . $filename . '"');
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: ' . filesize($filepath));
            header('Accept-Ranges: bytes');
            // Render the file
            readfile($filepath);
             
        }        
    }
}

?>