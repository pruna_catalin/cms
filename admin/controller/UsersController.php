<?php

class UsersController{
    /*
        Return List Array[Object]
    */
    public static function Grid($params = []){
            $grid_user = "";
            $count = 0;
            if(sizeOf($params) > 0){
                $user_list = AppSql::findAll("t1.*,t2.name,t2.address,t2.cnp,t2.city,t2.postal_code,t2.genere","`tbl_user` as `t1` left join `tbl_profile` as `t2` ON `t1`.id = `t2`.id_user",array(),$params[0].",".$params[1],"DESC");
            }else{
                $user_list = AppSql::findAll("t1.*,t2.name,t2.address,t2.cnp,t2.city,t2.postal_code,t2.genere","`tbl_user` as `t1` left join `tbl_profile` as `t2` ON `t1`.id = `t2`.id_user",array(),"0,5","DESC");
            }
            
            foreach($user_list as $item){
                $grid_user .= "<tr class='gradeX'><td>".$item->username."</td><td>".$item->name."</td><td>".$item->address."</td><td>".$item->cnp."</td><td>".$item->city."</td><td>".$item->postal_code."</td><td>".$item->genere."</td><td>
                    <a href='#accSettings1' role='button' class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='' onclick='editUser(".$item->id.");'> Edit
                    </a>
                    <a href='#' class='btn btn-danger btn-small hidden-phone' data-original-title='' onclick='delUser(".$item->id.");'>Delete</a>
                    </td></tr>";
                    $count++;
            }
            $view = new View("users",array("gridList" => $grid_user,"count"=> $count));
            return $view->render();
        
    }
    /*
        Return ListAjax Array[Object]
    */
    public static function GridAjax($params = []){
        $grid_user = "";
        $user_list = AppSql::findAll("t1.*,t2.name,t2.address,t2.cnp,t2.city,t2.postal_code,t2.genere","`tbl_user` as `t1` left join `tbl_profile` as `t2` ON `t1`.id = `t2`.id_user",array(),"0,5","DESC");
        foreach($user_list as $item){
            $grid_user .= "<tr class='gradeX'><td>".$item->username."</td><td>".$item->name."</td><td>".$item->address."</td><td>".$item->cnp."</td><td>".$item->city."</td><td>".$item->postal_code."</td><td>".$item->genere."</td><td>
                <a href='#accSettings1' role='button' class='btn btn-small btn-primary hidden-tablet hidden-phone' data-toggle='modal' data-original-title='' onclick='editUser(".$item->id.");'> Edit
                </a>
                <a href='#' class='btn btn-danger btn-small hidden-phone' data-original-title='' onclick='delUser(".$item->id.");'>Delete</a>
                </td></tr>";
        }
        return $grid_user;
    }
     public static function findById($id){
          $user_list = AppSql::findAll("t1.*,t2.name,t2.address,t2.cnp,t2.city,t2.postal_code,t2.genere","`tbl_user` as `t1` left join `tbl_profile` as `t2` ON (`t1`.id = `t2`.id_user) WHERE `t1`.id = ".$id."",array(),"0,25","DESC");
         
          echo  json_encode(array("success"=>true,"content"=> $user_list));
     }
     /*
        New User Form 
    */
     public static function newUser($data){
          $result = 0;
          $error = "";
          $user = $data['User'];
          $profile = $data['Profile'];
          if(strlen($user['password']) > 0 ){
              $user['password'] = md5($user['password']);
          }else{
              $user['password'] = md5("password");
          }
          $checkUsername = AppSql::findAll("*","`tbl_user`",array("username" => $user['username']),"0,25","DESC");  
          $checkEmail = AppSql::findAll("*","`tbl_user`",array("email" => $user['email']),"0,25","DESC");  
          if(is_object($checkUsername)){
              $error = "Username Is Already register!";
              $result = 1;
          }
          if(is_object($checkEmail)){
              $error = "Email Is Already register!";
              $result = 1;
          }
          $user['id_group'] = 5;
          $user['enable'] = 1;
          if($result == 0){
                $newUser =   AppSql::insert("`tbl_user`",$user);
                
            if($newUser > 0){
                $profile['id_user'] = $newUser;
                $newProfile =   AppSql::insert("`tbl_profile`",$profile);
            }
             echo json_encode(array("success"=>true,"content"=>UsersController::GridAjax(array()),"error"=>$error));
          }else{
               echo json_encode(array("success"=>true,"error"=>$error));
          }
         
     }
     /*
        Update User Form 
    */
      public static function update($data){
          $result = 0;
          $user = $data['User'];
          $profile = $data['Profile'];
          if(strlen($user['password']) == 0 ){
              unset($user['password']);
          }else{
              $user['password'] = md5($user['password']);
          }
          if(strlen($user['username']) == 0 ){
              unset($user['username']);
          }
          $check_auth = AppSql::findAll("*","`tbl_user`",array("email" => $user['email'],"id"=>$user['id']),"0,25","DESC");
            if(!is_object($check_auth)) {
                $check_email = AppSql::findAll("*","`tbl_user`",array("email" => $user['email']),"0,25","DESC");
               if(is_object($check_email)) { 
                    unset($user['username']);
               }else{
                    $result = 0; 
               }
            }else{
                 unset($user['username']);
            }
            AppSql::update("`tbl_user`",array("id"=>$user['id']), $user);
            AppSql::update("`tbl_profile`",array("id_user"=>$user['id']), $profile);
        echo json_encode(array("success"=>true,"content"=>UsersController::GridAjax(array())));
      }
      /*
      Delete user by ID
      
      */
      public static function delete($id){
          $result_user = AppSql::delete("`tbl_user`",array("id" => $id));
          $result_profile = AppSql::delete("`tbl_profile`",array("id_user" => $id));
          echo json_encode(array("result"=>$result_user,"content"=>UsersController::GridAjax(array())));
      }
    
}


?>