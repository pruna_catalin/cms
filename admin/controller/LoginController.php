<?php
session_start();
class LoginController{
    
    public static function CheckLogin(){
        $lang = "en";
        if(isset($_COOKIE['language'])){
            $lang = $_COOKIE['language'];
        }
        if(isset($_SESSION['loginAccess']) && $_SESSION['loginAccess'] == "Passed"){
                return true;
        }else{
           ThemeBase::Login();
        }
    }
    public static function Login($username,$password){
        if($username != "" && $password != ""){
            $username = htmlentities($username);
            $password = htmlentities($password);
            $sql = AppSql::findAll("*","tbl_user",array("username"=>$username,"password"=> md5($password),"superadmin"=>"1"),"0 , 25","");
            if(sizeof($sql) > 0){
                $_SESSION['loginAccess'] = "Passed";
                RedirectController::Redirect("index");
				return "Login Success";
            }else{
                $_SESSION['loginAccess'] = "";
				session_destroy();
				return "Login incorect";
            }
        }
    }
    public static function Logout(){
      $_SESSION['loginAccess'] = "";
      session_destroy();
      RedirectController::Redirect("login");
    }
}


?>