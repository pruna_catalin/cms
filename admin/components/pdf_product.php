<?php
require_once('./components/tcpdf/tcpdf.php');

class myPDF extends tcpdf {

    public function create_code($comanda, $produse) {
        if (!empty($comanda)) {
            $lot = str_pad($comanda->id, 6, 0, STR_PAD_LEFT);
            $id = str_pad(count($produse), 6, 0, STR_PAD_LEFT);

            return $lot . $id;
        }
    }
    public function mainbody2($id,$nume_oferta,$conditii_livrare,$termen1_livrare,$observatii,$modalitate_plata,$exp_date,$client) {
        $html = '<div style="text-align:center"><b>Anexa 1</b></div><br>';
        $html .='<table  style="width:520px;border:0px solid white; font-size:10px;border-bottom:0px solid black;">
                    
                       <tr>
                            <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:25px;">
                                Nr. CRT.<br>
                            </td>
                            <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:200px;">
                                Codul comanda al furnizorului
                            </td>
                            <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:45px;">
                                Cantitate<br>set/buc
                            </td>
                            
                            <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                                Valoare <br>(EUR)
                            </td>
                            <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                               Disponibilitate in
                               stocul furnizorului
                            </td>
                        </tr>
                    
'; 

        $suma = 0.00;
        $i=1;
        $j= 1;
        $produse = AppSql::findAll("*", "`prd_product`", array(":enable" => "1"), "0,100", "DESC");
        foreach ($produse as $item){
            $suma = $suma + $item->price;
             if($j > 80 && $j <= 81){
                 $j = 1;
                $html .='<tr style="border:5px;">
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:25px;">
                            Nr. CRT.<br>
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:200px;">
                            Codul comanda al furnizorului
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center;width:45px;">
                            Cantitate<br>set/buc
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            Pret unitar <br>(EUR)
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            Valoare <br>(EUR)
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                           Disponibilitate in
                           stocul furnizorului
                        </td>
                        
                    </tr>';
            }else{
                $html .= '
                    <tr>
                    <td style="border:0.2px solid black;font-weight:bolder;text-align:center;border-top:0px solid black">
                            '.$i.'.
                        </td>
                        <td colspan="1" style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            '.$item->name.'
                        </td>

                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            '.$item->quantity.'
                        </td>
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            '.$item->price.'
                        </td>
                        
                        <td style="border:0.2px solid black;font-weight:bolder;text-align:center">
                            '.$item->enable.'
                        </td>

                    </tr>
                    ';
                $i++;
                $j++;
            }
        }
        $html .= '<tr><td></td><td></td><td></td><td></td><td></td></tr><tr style=""><td></td><td></td><td></td><td></td><td style="text-align:center;font-size:20px;"><br><b>Total : '.$suma.' EUR</b></td></tr>
            ';
        $html .= "</table>" ;
        $html .= "</body></html>" ;
        $this->writeHTML($html, true, false, true, false, '');
    }
    
    public function mainbody1($id,$nume_oferta,$conditii_livrare,$termen1_livrare,$observatii,$modalitate_plata,$exp_date,$client) {



       
        $style = array(
            'position' => '',
            'align' => 'C',
            'stretch' => false,
            'fitwidth' => true,
            'width' => 10,
            'height' => 20,
            'cellfitalign' => '',
            'border' => false,
//            'hpadding' => 'auto',
//            'vpadding' => 'auto',
            'fgcolor' => array(0, 0, 0),
            'bgcolor' => false, //array(255,255,255),
            'text' => true,
            'font' => 'helvetica',
            'fontsize' => 4,
            'stretchtext' => 4
        );


        $html = "<html><body>";
        $html .= '<span style="width:300px;float:left;font-size:32px;">
                 <div style="text-align:center">SC Depozit Store SRL</div><br>
                 <table style="width:520px;font-size:10px;font-family">
                    <tr>
                        <td>
                            <table>
                                <tr>
                                    <td>In atentia :</td>
                                </tr>
                                <tr>
                                    <td>Domnule Utilizator</td>
                                </tr>
                                
                                <tr>
                                    <td>Adresa: </td>
                                </tr>
                                <tr>
                                    <td>Telefon: 0742323232</td>
                                </tr>
                                <tr>
                                    <td>Fax: 0742323232</td>
                                </tr>
                                <tr>
                                    <td>Email: user2gmail.com</td>
                                </tr>
                            </table>
                        </td>
                        
                        
                        
                         <td >
                            <table>
                                <tr>
                                    <td>Nume: Pruna Catalin</td>
                                </tr>
                                <tr>
                                    <td>Departament:Tehnico­comercial</td>
                                </tr>
                              
                                <tr>
                                    <td>Telefon: +40 725 823 770</td>
                                </tr>
                                <tr>
                                    <td>E­mail: prunacatalin@gmail.com</td>
                                </tr>
                                <tr>
                                    <td>Cererea dvs din data : '.date("y-m-d",time()).'</td>
                                </tr>
                                <tr>
                                    <td>Numar Oferta : '.$nume_oferta.'</td>
                                </tr>
                                <tr>
                                    <td>Data : '.date("y-m-d",time()).'</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>OFERTA nr.'.$nume_oferta.'</td>
                    </tr>
                     <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">Stimate  Utilizator,<br><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Va multumim pentru cererea dumneavoastra de oferta.<br>Cu speranta ca oferta noastra corespunde cerintelor dumneavoastra, asteptam cu mare interes comanda de aprovizionare.</td>
                    </tr>
                     <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Cu stima,<br><br>&nbsp;&nbsp;&nbsp;Pruna Catalin</td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td><b>Art. 1 Obiectul Ofertei</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">1.1   Obiectul ofertei este prezentat în tabelul anexat (Anexa 1).<br></td>
                    </tr>
                    <tr>
                        <td><b>Art. 2 Conditii de livrare</b></td>
                    </tr>
                    <tr>
                        <td>2.1   DDP '.ConditionDelivery.', conform INCOTERMS 2000.<br></td>
                    </tr>
                    <tr>
                        <td colspan="2"><b>Art. 3 Termen de livrare si informatii suplimentare despre livrare</b></td>
                    </tr>
                    <tr>
                        <td colspan="2">3.1   Termenul de livrare este de aproximativ '.DeliveryTime.' de la data lansarii comenzii ferme.</td>
                    </tr>
                    <tr>
                        <td colspan="2">3.2   '.Observation.'<br></td>
                    </tr>
                    <tr>
                        <td><b>Art. 4 Valoarea ofertei si conditii de plata</b></td>
                    </tr>
                    <tr>
                        <td>4.1   Valoarea ofertei este '.$valoare.' EUR fara TVA conform anexei 1.</td>
                    </tr>
                    <tr>
                        <td colspan="2">4.2   Facturarea produselor se va face la valoarea in EURO, in echivalentul in RON la cursul de schimb stabilit de catre BNR la data emiterii facturii.</td>
                    </tr>
                    <tr>
                        <td colspan="2">4.3   Plata se considera achitata în ziua in care DEPOZIT STORE poate dispune de aceasta.</td>
                    </tr>
                    <tr>
                        <td>4.4   Modalitate de plata: '.PaymentMode.'</td>
                    </tr>
                    <tr>
                        <td colspan="2">4.5   Toate spezele legate de efectuarea platii sunt si raman in sarcina cumparatorului, la banca acestuia.<br></td>
                    </tr>
                    <tr>
                        <td><b>Art. 5 Garantie</b></td>
                    </tr>
                    <tr>
                        <td>5.1   Termenul de garantie este de 12 luni de la livrare oferita de catre furnizor(Siemens AG).<br></td>
                    </tr>
                    <tr>
                        <td><b>Art. 6 Valabilitatea ofertei</b></td>
                    </tr>
                    <tr>
                        <td>6.1   Oferta este valabila pana pe data de '. date("y-m-d",time()).'.<br></td>
                    </tr>
                    <tr>
                        <td><b>Art. 7 Alte Clauze</b></td>
                    </tr>
                    <tr>
                        <td>7.1   Produsele ofertate se bazeaza pe lista de materiale pusa la dispozitie in cererea dvs.de oferta.<br></td>
                    </tr>
                    <tr>
                        <td><b>Art.8 Punere in functie</b></td>
                    </tr>
                    <tr>
                        <td>8.1   Punerea in functie a produselor nu este inclusa.</td>
                    </tr>
                    <tr>
                        <td colspan="2">8.2   Documentatia tehnica de integrare in sistemul existent nu este inclusa.</td>
                    </tr>
                    <tr>
                        <td colspan="2">8.3   Produsul se livreaza in ambalajul original, neparametrizat, cu setari,,factory settings ‘’.</td>
                    </tr>
                    <tr>
                        <td>8.4   Putem oferi suport tehnic on­site pentru instalare si parametrizare.</td>
                    </tr>
                    <tr>
                        <td colspan="2">8.5   Pretul pausal asociat unei ore de interventie on­site pentru un inginer este 40 eur fara TVA.</td>
                    </tr></table>';
        $html .= "</body></html>" ;        
        $this->writeHTML($html, true, false, true, false, '');
    }

    /*     * *
     *  override footer
     */
    public function header(){
        $this->SetPrintHeader(false);
    }
    public function footer() {
        
       
        // Position at 15 mm from bottom
      
        $this->SetY(-15);
       
        // Set font
        $this->SetFont('helvetica', 'I', 8);
        // Page number
        $this->Cell(0, 10, 'Pagina ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

    }
    public function colectData($id,$nume_oferta,$conditii_livrare,$termen1_livrare,$observatii,$modalitate_plata,$exp_date,$client){
        $this->SetTitle("Oferta Nr ".$nume_oferta);

        $this->AddPage();
        $this->mainbody1($id,$nume_oferta,$conditii_livrare,$termen1_livrare,$observatii,$modalitate_plata,$exp_date,$client);
        $this->AddPage();
        $this->mainbody2($id,$nume_oferta,$conditii_livrare,$termen1_livrare,$observatii,$modalitate_plata,$exp_date,$client);
        $this->SetFooterMargin(PDF_MARGIN_FOOTER);

        ob_end_clean();
        
        $this->Output("C:\\xampp\\htdocs\\it\\admin\\images\\pdf\\Oferta_Nr_1.pdf", 'FI');
       exit;
    }

}


?>