<div class="row-fluid">
<div class="span12">
    <div class="widget no-margin">
    <div class="widget-header">
        <div class="title">
        <span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span> <?php echo  Lang::message("Users");  ?>
        </div>
    </div>
    <div class="widget-body">
        <div id="dt_example" class="example_alt_pagination">
             <div id="custom-search-input">
                <div class="input-group col-md-8">
                    <input type="text" class="  search-query form-control" placeholder="<?php echo  Lang::message("Search");  ?>" />
                    <span class="input-group-btn">
                        <button class="btn btn-danger" type="button"><?php echo  Lang::message("Reset");  ?></button>
                    </span>
                    <a data-original-title="" class="btn btn-success" href="#createFilter" style="float:right" data-toggle="modal" onClick="resetUser();"><?php echo  Lang::message("NewUser");  ?></a>
                </div>
                
            </div><br>
        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
            <thead>
            <tr>
                <th style="width:10%"><?php echo  Lang::message("Username");  ?></th>
                <th style="width:15%"><?php echo  Lang::message("Name");  ?></th>
                <th style="width:15%"><?php echo  Lang::message("Address");  ?></th>
                <th style="width:15%" class="hidden-phone"><?php echo  Lang::message("Cnp");  ?></th>
                <th style="width:15%" class="hidden-phone"><?php echo  Lang::message("City");  ?></th>
                <th style="width:15%" class="hidden-phone"><?php echo  Lang::message("PostalCode");  ?></th>
                <th style="width:5%" class="hidden-phone"><?php echo  Lang::message("Sex");  ?></th>
                <th style="width:20%" class="hidden-phone"><?php echo  Lang::message("Options");  ?></th>
            </tr>
            </thead>
            <tbody id="changeContent">
                <?php 
                   echo $gridList;
                ?>
            </tbody>
           
        </table>
         <div id="data-table_paginate" class="dataTables_paginate paging_full_numbers">
             <a id="data-table_first" tabindex="0" class="first paginate_button paginate_button_disabled"><?php echo  Lang::message("First");  ?></a>
             <a id="data-table_previous" tabindex="0" class="previous paginate_button paginate_button_disabled"><?php echo  Lang::message("Previous");  ?></a>
             <span>
                 <?php
                    
                    $next = 25;
                    $itemsPerPage = 10;
                    $last = ceil($count / $itemsPerPage);
                    for($i=0;$i<=$count;$i++){
                        
                    }
                 
                 ?>
                 <a tabindex="0" class="paginate_active">1</a>
                 <a tabindex="0" class="paginate_button">2</a>
             </span>
             <a id="data-table_next" tabindex="0" class="next paginate_button"><?php echo  Lang::message("Next");  ?></a>
             <a id="data-table_last" tabindex="0" class="last paginate_button" href="index.php?case=users&pagination=<?php echo $last; ?>"><?php echo  Lang::message("Last");  ?></a>
        </div>
        <div class="clearfix"></div>
        </div>
    </div>
    </div>
</div>
<div id="createFilter" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="createFilterLabel" aria-hidden="true">
    <div class="modal-header">
        <h3 id="createFilterLabel"><?php echo  Lang::message("NewUser");  ?></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
        <div class="span12">
            <form class="form-horizontal  no-margin" action="javascript:void();" id="checkFormUser">
            <div class="control-group">
                <label class="control-label" for="inputName"><?php echo  Lang::message("Name");  ?></label>
                <div class="controls">
                <input type="text" id="inputName" name="Profile[name]" placeholder="<?php echo  Lang::message("Name");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputAddress"><?php echo  Lang::message("Address");  ?></label>
                <div class="controls">
                <input type="text" id="inputAddress" name="Profile[address]" placeholder="<?php echo  Lang::message("Address");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputCNP"><?php echo  Lang::message("CNP");  ?></label>
                <div class="controls">
                <input type="text" id="inputCNP" name="Profile[cnp]" placeholder="<?php echo  Lang::message("CNP");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputCity"><?php echo  Lang::message("City");  ?></label>
                <div class="controls">
                <input type="text" id="inputCity" name="Profile[city]" placeholder="<?php echo  Lang::message("City");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPostalCode"><?php echo  Lang::message("PostalCode");  ?></label>
                <div class="controls">
                <input type="text" id="inputPostalCode" name="Profile[postal_code]" placeholder="<?php echo  Lang::message("PostalCode");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputSex"><?php echo  Lang::message("Sex");  ?></label>
                <div class="controls">
                <label class="radio inline">
                    <input type="radio" id="inputSex" value="m" name="Profile[genere]" checked>M
                </label>
                <label class="radio inline">
                    <input type="radio" id="inputSex" value="f" name="Profile[genere]">F
                </label>
                
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputUsername"><?php echo  Lang::message("Username");  ?></label>
                <div class="controls">
                <input type="text" id="inputUsername" name="User[username]" placeholder="<?php echo  Lang::message("Username");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputPassword"><?php echo  Lang::message("Password");  ?></label>
                <div class="controls">
                <input type="password" id="inputPassword" name="User[password]" placeholder="<?php echo  Lang::message("Password");  ?>" class="required">
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" for="inputEmail"><?php echo  Lang::message("Email");  ?></label>
                <div class="controls">
                <input type="email" id="inputEmail" name="User[email]" placeholder="<?php echo  Lang::message("Email");  ?>" class="required">
                </div>
            </div>
             
            </form>
        </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">
        <?php echo  Lang::message("Close");  ?>
        </button>
        <button class="btn btn-primary" onClick="newUser();">
        <?php echo  Lang::message("Create");  ?>
        </button>
    </div>
</div>
               
<div id="accSettings1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
        ×
    </button>
    <h4 id="myModalLabel1">
        <?php echo  Lang::message("EditUser");  ?>
    </h4>
    </div>
    <form action="javascript:void();" method="POST" id="formUser">
    <div class="modal-body">
        <div class="row-fluid">  
            <div class="span4">
                <input type="hidden" class="span12" value="" id="id_user" name="User[id]">
                <input type="text" class="span12" placeholder="<?php echo  Lang::message("Name");  ?>" id="name" name="Profile[name]" required >
            </div>
            <div class="span4">
                <input type="text" class="span12" placeholder="<?php echo  Lang::message("Email");  ?>" id="email" name="User[email]" required>
            </div>
        </div>
        <div class="row-fluid">  
            <div class="span8">
            <input type="text" class="span12" placeholder="<?php echo  Lang::message("Address");  ?>"  id="address" name="Profile[address]">
            </div>
        </div>
        <div class="row-fluid">  
            <div class="span4">
                <input type="text" class="span12" placeholder="<?php echo  Lang::message("Username");  ?>"  id="username" name="User[username]" readonly>
            </div>
            <div class="span8">
            <input type="password" class="span12" placeholder="<?php echo  Lang::message("Password");  ?>" name="User[password]">
            </div>
        </div>
        </div>
      </form>
        <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">
        <?php echo  Lang::message("Close");  ?>
        </button>
        <button class="btn btn-primary" onClick="saveUser();">
            <?php echo  Lang::message("SaveChanges");  ?>
        </button>
    </div>
</div>
<script src="js/user.js"></script>