<div class="row-fluid">
     <div class="row-fluid">
            <div class="span12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe0b6;"></span> <?php echo  Lang::message("Settings");  ?>
                  </div>
                </div>
                <div class="widget-body">
                  <ul class="nav nav-tabs no-margin myTabBeauty">
                    <li class="active">
                      <a data-toggle="tab" href="#generalData">
                        <?php echo  Lang::message("GeneralData");  ?>
                      </a>
                    </li>
                    <li class="">
                      <a data-toggle="tab" href="#security">
                        <?php echo  Lang::message("SecurityData");  ?>
                      </a>
                    </li>
                  </ul>
                  <div class="tab-content" id="myTabContent">
                    <div id="generalData" class="tab-pane fade active in">
                      <div class="span12">
                        <div class="widget">
                        <div class="widget-header">
                            <div class="title">
                            <span class="fs1" aria-hidden="true" data-icon="&#xe023;"></span> <?php echo  Lang::message("Settings");  ?>
                            </div>
                        </div>
                        <div class="widget-body">
                            <form class="form-horizontal no-margin well">
                            <h5 class="text-info"><?php echo  Lang::message("GeneralInformation");  ?></h5>
                            <hr>
                            <div class="control-group">
                                <label class="control-label">
                                <?php echo  Lang::message("TitleSite");  ?>
                                </label>
                                <div class="controls">
                                <a href="#" id="firstName" data-type="text" data-pk="1" data-original-title="Click here to edit your first name" class="inputText editable editable-click">
                                    Srinu
                                </a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                <?php echo  Lang::message("MetaKey");  ?>
                                </label>
                                <div class="controls">
                                <a href="#" id="lastName" data-type="text" data-pk="1" data-original-title="Click here to edit your first name" class="inputText editable editable-click">
                                    Baswa
                                </a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                <?php echo  Lang::message("CustomLogo");  ?>
                                </label>
                                <div class="controls">
                                <a href="#" id="location" data-type="text" data-pk="1" data-original-title="Click here to edit your first name" class="inputText editable editable-click">
                                    Banglore, India.
                                </a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                Website URL
                                </label>
                                <div class="controls">
                                <a href="#" id="url" data-type="url" data-pk="1" data-original-title="Click here to edit your first name" class="inputText editable editable-click">
                                    http:www.abcxyz.com
                                </a>
                                </div>
                            </div>
                            <div class="control-group">
                                <label class="control-label">
                                Tags
                                </label>
                                <div class="controls">
                                <a href="#" id="tags" data-type="select2" data-pk="1" data-original-title="Enter tags" class="editable editable-click">
                                    Html, CSS, Javascript
                                </a>
                                </div>
                            </div>
                            
                            <div class="control-group">
                                <label class="control-label">
                                About
                                </label>
                                <div class="controls">
                                <a data-original-title="Write about your self" data-placeholder="Your comments here..." data-pk="1" data-type="textarea" id="aboutMe" href="#" class="inputTextArea editable editable-click" style="margin-bottom: 10px;">
                                    About me :)
                                </a>
                                </div>
                            </div>
                            <div class="form-actions no-margin">
                                <button type="submit" class="btn btn-info">
                                Save
                                </button>
                                <button type="button" class="btn">
                                Cancel
                                </button>
                            </div>
                            </form>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div id="security" class="tab-pane fade">
                      <p>
                        Raw denim you probably haven't heard of them jean shorts Austin. Nesciunt tofu stumptown aliqua, retro synth master cleanse. Reprehenderit butcher retro keffiyeh dreamcatcher synth terry richardsoAustin. Nesciunt tofu stumptown aliqua, retro synth master cleanse.
                      </p>
                      <p>
                        Mustache cliche tempor, williamsburg carles vegan helvetica.prehenderit butcher retro keffiyeh dreamcatcher synth. Cosby Reprehenderit butcher retro keffiyeh dreamcatcher synth. Cosby sweater eu banh mi, qui irure terry richardson ex squid.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
</div>