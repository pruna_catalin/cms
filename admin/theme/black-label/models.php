<div class="row-fluid">
     <div class="row-fluid">
            <div class="span12">
              <div class="widget">
                <div class="widget-header">
                  <div class="title">
                    <span class="fs1" aria-hidden="true" data-icon="&#xe0b6;"></span> <?php echo  Lang::message("Settings");  ?>
                  </div>
                </div>
                <div class="widget-body">
                    <a data-original-title="" class="btn btn-success" href="#createFilter" style="float:right" data-toggle="modal" onClick="resetModel();"><?php echo  Lang::message("NewModel");  ?></a>
                  <ul class="nav nav-tabs no-margin myTabBeauty">
                    <li class="active">
                      <a data-toggle="tab" href="#generalData">
                        <?php echo  Lang::message("ListModels");  ?>
                      </a>
                    </li>
                  </ul>
                  
                  <div class="tab-content" id="myTabContent">
                    <div id="generalData" class="tab-pane fade active in">
                      <div class="span12">
                        <div class="widget">
                        <table class="table table-condensed table-striped table-hover table-bordered pull-left" id="data-table">    
                            <thead>
                            <tr>
                                <th style="width:35%"><?php echo  Lang::message("Name");  ?></th>
                                <th style="width:35%"><?php echo  Lang::message("Number");  ?></th>
                                <th style="width:20%" class="hidden-phone"><?php echo  Lang::message("Options");  ?></th>
                            </tr>
                            </thead>
                            <tbody id="changeContent">
                                <?php 
                                echo $gridList;
                                ?>
                            </tbody>
                        
                        </table>
                        
                        </div>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
    
</div>
<div id="accSettings1" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1" aria-hidden="true">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            ×
        </button>
        <h4 id="myModalLabel1">
            <?php echo  Lang::message("EditModel");  ?>
        </h4>
    </div>
    <form action="javascript:void();" method="POST" id="formUser">
    <div class="modal-body">
        <div class="row-fluid">  
            <div class="span4">
                <input type="hidden" class="span12" value="" id="id_model" name="Models[id]">
                <input type="text" class="span12" placeholder="<?php echo  Lang::message("Name");  ?>" id="name" name="Models[name]" required >
            </div>
            <div class="span4">
                <input type="text" class="span12" placeholder="<?php echo  Lang::message("Number");  ?>" id="nr" name="Models[nr]" required>
            </div>
        </div>
      </form>
        <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">
            <?php echo  Lang::message("Close");  ?>
            </button>
            <button class="btn btn-primary" onClick="saveModel();">
                <?php echo  Lang::message("SaveChanges");  ?>
            </button>
        </div>
    </div>
</div>
<div id="createFilter" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="createFilterLabel" aria-hidden="true">
    <div class="modal-header">
        <h3 id="createFilterLabel"><?php echo  Lang::message("NewModel");  ?></h3>
    </div>
    <div class="modal-body">
        <div class="row-fluid">
            <div class="span12">
                <form class="form-horizontal  no-margin" action="javascript:void();" id="checkFormModel">
                    <div class="control-group">
                        <label class="control-label" for="inputName"><?php echo  Lang::message("Name");  ?></label>
                        <div class="controls">
                            <input type="text" id="inputName" name="Models[name]" placeholder="<?php echo  Lang::message("Name");  ?>" class="required">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="inputNr"><?php echo  Lang::message("Number");  ?></label>
                        <div class="controls">
                            <input type="text" id="inputNr" name="Models[nr]" placeholder="<?php echo  Lang::message("Number");  ?>" class="required">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">
        <?php echo  Lang::message("Close");  ?>
        </button>
        <button class="btn btn-primary" onClick="newModel();">
        <?php echo  Lang::message("Create");  ?>
        </button>
    </div>
</div>
<script src="js/model.js"></script>