<!DOCTYPE html>
    <html lang="en">
      <head>
        <meta charset="utf-8">
        <title><?php echo  Lang::message("TitleAdminLogin");  ?></title>
        <meta name="author" content="Srinu Basava">
        <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
        <meta name="description" content="Black Label Admin Admin UI">
        <meta name="keywords" content="Black Label Admin, Admin UI, Admin Dashboard, Srinu Basava">
        <script src="js/html5-trunk.js"></script>
        
        <script src="js/alertify.js"></script>
        <script src="js/jquery.min.js"></script>
        <script src="js/fileinput/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
        <!-- sortable.min.js is only needed if you wish to sort / rearrange files in initial preview.
            This must be loaded before fileinput.min.js -->
        <script src="js/fileinput/plugins/sortable.min.js" type="text/javascript"></script>
        <!-- purify.min.js is only needed if you wish to purify HTML content in your preview for HTML files.
            This must be loaded before fileinput.min.js -->
        <script src="js/fileinput/plugins/purify.min.js" type="text/javascript"></script>
        <!-- the main fileinput plugin file -->
        <script src="js/fileinput/fileinput.min.js"></script>
        <script src="js/jquery-ui-1.8.23.custom.min.js"></script>
        <script src="js/upload_images/plupload.full.min.js"></script>
        <script src="js/upload_images/jquery.plupload.queue/jquery.plupload.queue.min.js"></script>
        <link href="icomoon/style.css" rel="stylesheet">
        <!-- bootstrap css -->
        <link href="css/main.css" rel="stylesheet">
        <link href="css/fullcalendar.css" rel="stylesheet">
        <link href="js/upload_images/jquery.plupload.queue/css/jquery.plupload.queue.css" rel="stylesheet">
        <link href="css/wysiwyg/bootstrap-wysihtml5.css" rel="stylesheet">
        <link href="css/wysiwyg/wysiwyg-color.css" rel="stylesheet">
        <link href="css/timepicker.css" rel="stylesheet">
        <link href="css/bootstrap-editable.css" rel="stylesheet">
        <link href="css/select2.css" rel="stylesheet">
        <link href="css/select2.min.css" rel="stylesheet">
        <link href="css/alertify.css" rel="stylesheet">
        <link href="css/style.css" rel="stylesheet">
      
      </head>
  <body>
    <header>
      <a href="index.php?case=index" class="logo"><?php echo  Lang::message("TitleAdminLogin");  ?></a>
      <div id="mini-nav">
        <ul class="hidden-phone">
          <li><a href="timeline.html" >Tasks</a></li>
          <li><a href="#">Signup's <span id="newSignup">06</span></a></li>
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              Messages <span id="messagesCountDown">21</span>
              <b class="caret icon-white"></b>
            </a>
            <ul class="dropdown-menu pull-right">
              <li class="quick-messages">
                <img src="img/avatar-1.png" class="avatar" alt="Avatar">
                <div class="message-date text-info">
                  <i>02 <span class="month">mins</span></i>
                </div>
                <div class="message-wrapper">
                  <h4 class="message-heading">Ubiquitous customized</h4>
                  <p class="message">
                    Enable impactful niches engage impactful 
                  </p>
                </div>
              </li>
              <li class="quick-messages">
                <img src="img/avatar-2.png" class="avatar" alt="Avatar">
                <div class="message-date text-info">
                  <i>02 <span class="month">mins</span></i>
                </div>
                <div class="message-wrapper">
                  <h4 class="message-heading">Ubiquitous customized</h4>
                  <p class="message">
                    Enable impactful niches engage impactful 
                  </p>
                </div>
              </li>
              <li class="quick-messages">
                <img src="img/avatar-5.png" class="avatar" alt="Avatar">
                <div class="message-date text-info">
                  <i>38 <span class="month">mins</span></i>
                </div>
                <div class="message-wrapper">
                  <h4 class="message-heading">Ubiquitous customized</h4>
                  <p class="message">
                    Enable impactful niches engage impactful 
                  </p>
                </div>
              </li>
              <li class="quick-messages">
                <img src="img/avatar-4.png" class="avatar" alt="Avatar">
                <div class="message-date text-info">
                  <i>27 <span class="month">Apr</span></i>
                </div>
                <div class="message-wrapper">
                  <h4 class="message-heading">Ubiquitous customized</h4>
                  <p class="message">
                    Enable impactful niches engage impactful 
                  </p>
                </div>
              </li>
              <li class="quick-messages">
                <img src="img/avatar-6.png" class="avatar" alt="Avatar">
                <div class="message-date text-info">
                  <i>18 <span class="month">Apr</span></i>
                </div>
                <div class="message-wrapper">
                  <h4 class="message-heading">Ubiquitous customized</h4>
                  <p class="message">
                    Enable impactful niches engage impactful 
                  </p>
                </div>
              </li>
            </ul>
          </li>
          <li><a href="index.php?case=logout"><?php echo  Lang::message("Logout");  ?></a></li>
        </ul>
      </div>
    </header>

    <div class="container-fluid">
      <div id="mainnav" class="hidden-phone hidden-tablet">
        <ul>
          <li class="<?php echo ($selectMenu == "1")?  'active':''; ?>">
            <?php if($selectMenu == "1") echo '<span class="current-arrow"></span>'; ?>
            <a href="index.php?case=index">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0a1;"></span>
              </div>
              <?php echo  Lang::message("Dashboard");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "2")?  'active':''; ?>">
            <?php if($selectMenu == "2") echo '<span class="current-arrow"></span>'; ?>
            <a href="charts.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe097;"></span>
              </div>
               <?php echo  Lang::message("Statistics");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "3")?  'active':''; ?>">
            <?php if($selectMenu == "3") echo '<span class="current-arrow"></span>'; ?>
            <a href="timeline.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe047;"></span>
              </div>
              <?php echo  Lang::message("Returns");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "4")?  'active':''; ?>">
            <?php if($selectMenu == "4") echo '<span class="current-arrow"></span>'; ?>
            <a href="index.php?case=users">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe14a;"></span>
              </div>
              <?php echo  Lang::message("Users");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "5")?  'active':''; ?>">
            <?php if($selectMenu == "5") echo '<span class="current-arrow"></span>'; ?>
            <a href="index.php?case=products">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0b8;"></span>
              </div>
              <?php echo  Lang::message("Products");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "6")?  'active':''; ?>">
            <?php if($selectMenu == "6") echo '<span class="current-arrow"></span>'; ?>
            <a href="index.php?case=models">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0d2;"></span>
              </div>
              <?php echo  Lang::message("Models");  ?>
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "7")?  'active':''; ?>">
            <?php if($selectMenu == "7") echo '<span class="current-arrow"></span>'; ?>
            <a href="grid.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe14c;"></span>
              </div>
              Grid
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "8")?  'active':''; ?>">
            <?php if($selectMenu == "8") echo '<span class="current-arrow"></span>'; ?>
            <a href="gallery.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe00d;"></span>
              </div>
              Gallery
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "9")?  'active':''; ?>">
            <?php if($selectMenu == "9") echo '<span class="current-arrow"></span>'; ?>
            <a href="typography.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe100;"></span>
              </div>
              Typography
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "10")?  'active':''; ?>">
            <?php if($selectMenu == "10") echo '<span class="current-arrow"></span>'; ?>
            <a href="icons.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0a9;"></span>
              </div>
              Icons
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "11")?  'active':''; ?>">
            <?php if($selectMenu == "11") echo '<span class="current-arrow"></span>'; ?>
            <a href="error.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0f4;"></span>
              </div>
              404
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "12")?  'active':''; ?>">
            <?php if($selectMenu == "12") echo '<span class="current-arrow"></span>'; ?>
            <a href="login.html">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe088;"></span>
              </div>
              Login
            </a>
          </li>
          <li class="<?php echo ($selectMenu == "13")?  'active':''; ?>">
            <?php if($selectMenu == "13") echo '<span class="current-arrow"></span>'; ?>
            <a href="index.php?case=settings">
              <div class="icon">
                <span class="fs1" aria-hidden="true" data-icon="&#xe0b5;"></span>
              </div>
              <?php echo  Lang::message("Settings");  ?>
            </a>
          </li>
        </ul>
      </div>
      
      <div class="dashboard-wrapper">
        <div class="main-container">
         

          <div class="row-fluid">
            <div class="span12">
              <ul class="breadcrumb-beauty">
                <li>
                  <a href="?case=index"><span class="fs1" aria-hidden="true" data-icon="&#xe002;"></span> Dashboard</a>
                </li>
                <li>
                  <a href="#"></a>
                </li>
              </ul>
            </div>
          </div>

          <br>
          <?php echo $content; ?>
        </div>
      </div><!-- dashboard-container -->
    </div><!-- container-fluid -->
    <footer>
      <p class="copyright">&copy; Black Label Admin 2013</p>
    </footer>
    
    
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript" src="js/bootstrap-timepicker.js"></script>
    <script src="js/bootstrap-editable.min.js"></script>
	<script src="js/notify.min.js"></script>	
    
    <script src="js/wysiwyg/wysihtml5-0.3.0.js"></script> 
     <script src="js/wysiwyg/bootstrap-wysihtml5.js"></script>
    <!-- morris charts -->
    <script src="js/morris/morris.js"></script>
    <script src="js/morris/raphael-min.js"></script>

    <!-- Flot charts -->
    <script src="js/flot/jquery.flot.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>

    <!-- Calendar Js -->
    <script src="js/fullcalendar.js"></script>

    <!-- Tiny Scrollbar JS -->
    <script src="js/tiny-scrollbar.js"></script>

    <!-- custom Js -->
    <script src="js/custom-index.js"></script>
    <script src="js/custom.js"></script>
    <script src="js/custom-forms.js"></script>
    <script src="./js/bootbox.min.js"></script>
    <script type="text/javascript" src="js/date-picker/date.js"></script>
    <script type="text/javascript" src="js/date-picker/daterangepicker.js"></script>
    <script src="js/select2.js"></script>
    <script src="js/fileinput/plugins/canvas-to-blob.min.js" type="text/javascript"></script>
    <script src="js/fileinput/fileinput.min.js"></script>
    <script src="js/fileinput/fileinput_locale_uk.js"></script>
    
    
    <!-- Editable Inputs -->
    
    
  </body>
</html>