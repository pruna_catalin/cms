<!DOCTYPE html>
  <!--[if lt IE 7]>
    <html class="lt-ie9 lt-ie8 lt-ie7" lang="en">
  <![endif]-->

  <!--[if IE 7]>
    <html class="lt-ie9 lt-ie8" lang="en">
  <![endif]-->

  <!--[if IE 8]>
    <html class="lt-ie9" lang="en">
  <![endif]-->

  <!--[if gt IE 8]>
    <!-->
    <html lang="en">
    <!--
  <![endif]-->

  <head>
    <meta charset="utf-8">
    <title><?php echo  Lang::message("TitleAdminLogin");  ?></title>
    <meta name="author" content="Pruna Catalin">
    <meta content="width=device-width, initial-scale=1.0, user-scalable=no" name="viewport">
    <meta name="description" content="<?php echo  Lang::message("TitleAdminLogin");  ?>">
    <meta name="keywords" content="<?php echo  Lang::message("TitleAdminLogin");  ?>">
    <script src="js/html5-trunk.js"></script>
    <link href="icomoon/style.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
    <!--[if lte IE 7]>
      <script src="css/icomoon-font/lte-ie7.js"></script>
    <![endif]-->
    
  </head>
  <body>
    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span4 offset4">
          <div class="signin">
            <h1 class="center-align-text"><?php echo  Lang::message("TitleAdminLogin");  ?></h1>
            <form action="index.php?case=login" class="signin-wrapper" method="post">
              <div class="content">
				<div class="error_login"><?php echo $errors; ?></div>
                <input class="input input-block-level" placeholder="<?php echo  Lang::message("Username");  ?>" type="text" value="" name="username">
                <input class="input input-block-level" placeholder="<?php echo  Lang::message("Password");  ?>" type="password" name="password">
              </div>
              <div class="actions">
                <input class="btn btn-info pull-right" type="submit" value="<?php echo  Lang::message("Login");  ?>" name="CheckMe">
                <span class="checkbox-wrapper">
					
                  <a href="index.html" class="pull-left"><?php echo  Lang::message("ForgotP");  ?></a>
                </span>
                <div class="clearfix"></div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    
  </body>
</html>