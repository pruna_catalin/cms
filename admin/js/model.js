function saveModel(){
	var data = $("#formUser").serialize();
	var id =  $("#id_model").val();
	 $.ajax({
			type: 'post',
			url: 'index.php?case=models&update='+id,
			data: data,
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					bootbox.alert("Success Saved", function() {});
					$("#changeContent").html(data_result.content);
				}
			}
    });
	
}
function newModel(){
	var data = $("#checkFormModel").serialize();
	$.ajax({
			type: 'post',
			url: 'index.php?case=models&new=true',
			data: data,
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					if(data_result.error.length > 0){
						bootbox.alert(data_result.error, function() {});
					}else{
						bootbox.alert("Success Saved", function() {});
						$("#changeContent").html(data_result.content);
					}
				}
			}
    });
}
function resetModel(){
	$("#inputName").val("");
	$("#inputNr").val("");
	
}
function delModel(id){
	bootbox.confirm("Are you sure to want to delete this?", function(result) {                
		if (result) {                                             
			 $.ajax({
					type: 'post',
					url: 'index.php?case=models&delete='+id,
					data: data,
					dataType: 'json',
					success: function(data_result){
						if(data_result.success) {
							bootbox.alert("Success Deleted", function() {});
							$("#changeContent").html(data_result.content);
						}else{
							bootbox.alert("This record is not exists", function() {});
							
						}
					}
			});                              
		}
	});
}
function editModel(id){
   $("#id_model").val(id);
   $.ajax({
			type: 'post',
			url: 'index.php?case=models&findById='+id,
			data:"",
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					result = data_result.content;
					$("#id_model").val(id);
					$("#name").val(result[0].name);
					$("#nr").val(result[0].nr);
					
				}
			}
    });
}
