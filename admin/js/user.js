function saveUser (){
	var data = $("#formUser").serialize();
	var id =  $("#id_user").val();
	 $.ajax({
			type: 'post',
			url: 'index.php?case=users&update='+id,
			data: data,
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					bootbox.alert("Success Saved", function() {});
					$("#changeContent").html(data_result.content);
				}
			}
    });
	
}
function newUser(){
	var data = $("#checkFormUser").serialize();
	$.ajax({
			type: 'post',
			url: 'index.php?case=users&new=true',
			data: data,
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					if(data_result.error.length > 0){
						bootbox.alert(data_result.error, function() {});
					}else{
						bootbox.alert("Success Saved", function() {});
						$("#changeContent").html(data_result.content);
					}
				}
			}
    });
}
function resetUser(){
	$("#inputName").val("");
	$("#inputAddress").val("");
	$("#inputCNP").val("");
	$("#inputCity").val("");
	$("#inputUsername").val("");
	$("#inputPassword").val("");
	$("#inputEmail").val("");
}
function delUser (id){
	bootbox.confirm("Are you sure to want to delete this?", function(result) {                
		if (result) {                                             
			 $.ajax({
					type: 'post',
					url: 'index.php?case=users&delete='+id,
					data: data,
					dataType: 'json',
					success: function(data_result){
						if(data_result.result > 0) {
							bootbox.alert("Success Deleted", function() {});
							$("#changeContent").html(data_result.content);
						}else{
							bootbox.alert("This record is not exists", function() {});
							
						}
					}
			});                              
		}
	});
}
function editUser(id){
   $("#id_user").val(id);
   $.ajax({
			type: 'post',
			url: 'index.php?case=users&findById='+id,
			data:"",
			dataType: 'json',
			success: function(data_result){
				if(data_result.success) {
					result = data_result.content;
					$("#id_user").val(id);
					$("#name").val(result[0].name);
					$("#email").val(result[0].email);
					$("#username").val(result[0].username);
					$("#address").val(result[0].address);
				}
			}
    });
}
