
$(document).ready(function() {
  $(".js-example-responsive").select2();
  $("#input-id").fileinput({'showUpload':false, 'previewFileType':'any'});
});
var images = [];
function saveProduct(){
    
    var data = $("#formProduct").serialize();
    $.ajax({
        type: 'post',
        url: 'index.php?case=products&add=new',
        data: data,
        dataType: 'json',
        success: function(data_result){
          if(data_result.success) {
            $.notify("Success Saved", "success");
            $("#changeContent").html(data_result.content);
          }
        }
      });
}


 $("#html5_uploader").pluploadQueue({
        // General settings
        runtimes : 'html5',
        url : "index.php?case=products&upload=true",
        chunk_size : '1mb',
        unique_names : true,
         multiple_queues : true,
        filters : {
            max_file_size : '10mb',
            mime_types: [
                {title : "Image files", extensions : "jpg,gif,png,jpeg"}
            ]
        },
 
        // Resize images on clientside if we can
        //resize : {width : 320, height : 240, quality : 100}
    });
 