
function addDropZoneListener() {
	var url_param = getParamsUrl();	

	Dropzone.autoDiscover = false;
	
	$("#dropzoneMaison").dropzone({ 
		url: 'controller/gestion_ajax.php',
		acceptedFiles: '.jpeg,.jpg,.png,.gif',
		maxFiles: 6,
		addRemoveLinks: true,
		//dictRemoveFileConfirmation: "Delete this file?",
		init: function() {
			$("#dropzoneMaison").data('dz', this);
			
			var pathFiles = 'files/images/thumbnail/';
			
			this.on("addedfile", function(file) {
				if (!file.name.match(/\.jpeg$/i) && !file.name.match(/\.jpg$/i) &&
						!file.name.match(/\.gif$/i) && !file.name.match(/\.png$/i)) {
					
					if(file.name.match(/\.xls$/i) || file.name.match(/\.xlsx$/i))
						this.emit("thumbnail", file, "images/excel_large.png");
					else if(file.name.match(/\.ppt$/i) || file.name.match(/\.pptx$/i))
						this.emit("thumbnail", file, "images/powerpoint_large.png");
					else if(file.name.match(/\.pdf$/i))
						this.emit("thumbnail", file, "images/pdf_large.png");
					else if(file.name.match(/\.doc$/i) || file.name.match(/\.docx$/i))
						this.emit("thumbnail", file, "images/word_large.png");
					else
						this.emit("thumbnail", file, "images/unknown_large.png");
				}else {
					if(typeof file.filename !== 'undefined')
						this.emit("thumbnail", file, pathFiles + "/" + file.filename);
				}
			});
			
			this.on("permaremovefile", function(file) {
				var id = $("#select_id").val();
				var etat_maison = $("#select_etat").val();
 				
				$.ajax({
					type: 'post',
					url: 'controller/gestion_ajax.php',
					data: "action=removeFile&item=" + url_param['item'] + 
							'&maison_id=' + id + '&etat_maison=' + etat_maison+ '&file_name='+ file.filename,
					dataType: 'json',
					success: function (response) {
						if(id === '')
							$('.TAGGED_'+file.filename.split(".", 2)[0]).remove();
					},
					error: function (retour_php) {
						cute_alert_box("[R][DropZone] Erreur avec la communication serveur.", "alert_error");
					}
				});
			});
			
			this.on("sending", function(file, xhr, formData) {
				var id = $("#select_id").val();
				var etat_maison = $("#select_etat").val();
				// Will send the data along with the file as POST data.
				formData.append("action", 'uploadFiles');
				formData.append("item", url_param['item']);
				formData.append("maison_id", id);
				formData.append("etat_maison", etat_maison);
			});
			
			var idInc = 0;
			this.on("success", function(file, responseText) {
				var response = JSON.parse(responseText);
				
				file.filename = response.files[0]['filename'];
				if($("#select_id").val() === '') {
					$("#dropzoneMaison")
						.before('<input type="hidden" class="TAGGED_'+file.filename.split(".", 2)[0]+'" name="files['+idInc+'][name]" value="'+file.name+'"/>')
						.before('<input type="hidden" class="TAGGED_'+file.filename.split(".", 2)[0]+'" name="files['+idInc+'][filename]" value="'+file.filename+'"/>')
						.before('<input type="hidden" class="TAGGED_'+file.filename.split(".", 2)[0]+'" name="files['+idInc+++'][size]" value="'+file.size+'"/>');
				}
			});
		}
	});
}

function updateDropZone() {
	var url_param = getParamsUrl();	
	
	var maison_id = $("#select_id").val();
	var etat_maison = $("#select_etat").val();
	
	
	
	$("#dropzoneMaison").data('dz').removeAllFiles();
	
	if(maison_id !== '') {
		$.ajax({
			type: 'post',
			url: 'controller/gestion_ajax.php',
			data: "action=readFiles&item=" + url_param['item'] + '&maison_id=' + maison_id + '&etat_maison=' + etat_maison,
			dataType: 'json',
			success: function (retour_php) {
				if (retour_php.success === false) {
					cute_alert_box(retour_php.erreur, "alert_error");
				} else {
					for(var file in retour_php) {
						var mockFile = retour_php[file];

						// Call the default addedfile event handler
						$("#dropzoneMaison").data('dz').emit("addedfile", mockFile);
						// Make sure that there is no progress bar, etc...
						$("#dropzoneMaison").data('dz').emit("complete", mockFile);
						$("#dropzoneMaison").data('dz').files.push(mockFile);
					}

					$("#dropzoneMaison").data('dz').options.maxFiles -= retour_php.length;
				}
			},  
			error: function (retour_php) {
				cute_alert_box("[L][DropZone] Erreur avec la communication serveur.", "alert_error");
			}
		});
	}
}
//removeAllFiles
