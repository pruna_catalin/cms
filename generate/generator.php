<?php

include "../includes/config.php";
$query_get_tables = "SELECT * FROM information_schema.tables WHERE table_schema = '" . $database . "' ORDER BY table_name;";
include "./GenerateModels.php";

CombineTableAndColumns();

function get_tables($sql) {
    global $connexion;
    $result = $connexion->prepare($sql);
    $result->execute();
    return parseTables($result->fetchAll(PDO::FETCH_OBJ));
}

function get_columns($result) {
    $columns = array();
    global $connexion;
    global $database;
    foreach ($result as $table) {
        $query_get_columns = "SELECT *
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_SCHEMA = '" . $database . "' AND TABLE_NAME = '" . $table . "'";
        $result = $connexion->prepare($query_get_columns);
        $result->execute();
        $columns[] = parseColumns($result->fetchAll(PDO::FETCH_OBJ));
    }
    return $columns;
}

function parseColumns($result) {
    $columns = array();
    foreach ($result as $table => $key) {
        $columns[] = array($key->COLUMN_NAME => array("type" => $key->DATA_TYPE, "is_null" => $key->IS_NULLABLE, "length" => $key->COLUMN_TYPE));
    }
    return $columns;
}

function parseTables($result) {
    $tables = array();
    foreach ($result as $table => $key) {
        $tables[] = $key->TABLE_NAME;
    }
    return $tables;
}

function CombineTableAndColumns() {
    global $query_get_tables;
    $combineTabel = array();
    $tables = get_tables($query_get_tables);
    $columns = get_columns($tables);
    $typeColumns = array();
    $columnArr = array();
    for ($i = 0; $i < sizeOf($columns); $i++) {
        foreach ($columns[$i] as $item) {
            foreach ($item as $sub_item => $key) {
                $typeColumns[] = array($sub_item => $key);
            }
        }
        $combineTabel[] = array($tables[$i] => $typeColumns);
    }
    GenerateModels($combineTabel);
//    echo "<pre>";
//    print_R($combineTabel);
//    echo "</pre>";
}

function GenerateModels($combineTable) {
    global $database, $query_get_tables, $connexion;

    $tables_relations = array();
    $tables_relations_return = array();
    $tables = get_tables($query_get_tables);
    for ($i = 0; $i < sizeOf($tables); $i++) {
        $queryRelations = 'select * from information_schema.key_column_usage where constraint_schema = "' . $database . '" AND TABLE_NAME="' . $tables[$i] . '"';
        $result = $connexion->prepare($queryRelations);
        $result->execute();
        $tables_relations[] = $result->fetchAll(PDO::FETCH_OBJ);
    }
    for ($i = 0; $i < sizeOf($tables_relations); $i++) {
        for ($j = 0; $j < sizeOf($tables_relations[$i]); $j++) {
            $tables_relations_return[] = array("table_name" => $tables_relations[$i][$j]->TABLE_NAME, "reference_key" => $tables_relations[$i][$j]->COLUMN_NAME, "constraint_name" => $tables_relations[$i][$j]->CONSTRAINT_NAME);
        }
    }

    $parseRelations = parseRelations($tables_relations_return, $tables);

    return $tables_relations_return;
}

function parseRelations($parseRelations, $tables) {
    $content = "array(";
    for ($i = 0; $i < sizeOf($tables); $i++) {
        for ($j = 0; $j < sizeOf($parseRelations); $j++) {


            if ($tables[$i] == $parseRelations[$j]['table_name']) {
                if ($j == 0) {
                    $content .= "'" . $parseRelations[$j]['reference_key'] . "'=>";
                } else {
                    $content .= "'" . $parseRelations[$j]['reference_key'] . "',";
                }
            }
        }
        echo $content;
        exit;
    }

    echo "<pre>";
    print_R($content);
    echo "</pre>";
}

?>
