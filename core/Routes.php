<?php


$rules =  array(
		"BaseRules" =>array(
                                    "url"=>array("index","product","account","users"),
                                    "actions"=>array("edit","new","delete","view","update","pag"),
                                    "method"=>array("post","get")
                                    
                                    ),
                "SpecialRules"=>array(
                                    "adminActions" => array("\/edit\/[0-9]","\/delete\/[0-9]","\/view\/[0-9]","\/update\/[0-9]","\/pag\/[0-9]\-[0-9]")
                    
                ),
		"PathRules" =>array(
                                    "LivePath" => "http://localhost/cms",
                                    "JsAdmin" => "/admin/js/",
                                    "CssAdmin" => "/admin/css/",
                                    "ThemeFront" => "/themes/",
                                    ),
		"ThemeName" => array("name"=> "automarket")

	);

?>