<?php

include "LibraryView/View.php";
$content = "";

class SiteController {

	public static function actionIndex() {
		global $content;
		$view = new View("index", array("countUsers" => $users));
		return $view->render();
	}

	public static function findById($id) {
		$recent_product = AppSql::findAll("`t1.*` ,t2.name as name_model", "prd_product as `t1` LEFT JOIN prd_model as t2  ON `t1.id_model`=t2.id", array("`t1.id`" => $id), "0,1", "DESC", "`t1.id`");
		// print_R($recent_product);exit;
		$images_front = AppSql::findAll("*", "prd_image", array("id_product" => $recent_product[0]->id), "0,25", "ASC", "id");
		$view = new View("product", array("recent_products" => $recent_product, "images_front" => $images_front));
		return $view->render();
	}

	public static function actionProduct() {
		global $content;
		$usersSubscribe = AppSql::Count("", "subscribe", array())[0];
		$recent_product = AppSql::findAll("t1.*,t2.name as name_model", "prd_product as t1 LEFT JOIN prd_model as t2  ON t1.id_model=t2.id", array(), "0,25", "DESC", "t1.id");
		$images_front = AppSql::findAll("*", "prd_image", array("id_product" => $recent_product[0]->id), "0,25", "ASC", "id");
		$view = new View("index", array("content" => $recent_product, "recent_products" => $recent_product, "images_front" => $images_front, "countUsers" => $usersSubscribe));
		return $view->render();
	}

	public static function actionSubscribe($items) {
		$models = $items['SubScribe'];
		$models['create_at'] = date("Y-m-d", time());

		$newSub = AppSql::insert("`subscribe`", $models);
		return json_encode(array("success" => true, "data_result" => "Success Add!"));
	}

	public static function actionAccount() {
		$view = new View("account", array());
		return $view->render();
	}

	public static function actionListProduct($type, $params) {
		$list = "";
		if ($type == "pc") {
			$list_Arr = AppSql::findAll("t1.*,t2.name as name_model", "prd_product as t1 LEFT JOIN prd_model as t2  ON t1.id_model=t2.id", array(), "0,25", "DESC", "t1.id");

			foreach ($list_Arr as $item) {
				$images_front = AppSql::findAll("*", "prd_image", array("id_product" => $item->id), "0,25", "ASC", "id");
				if (sizeof($images_front) == 0) {
					$images_front = App::BasePath() . "/images/default.jpg";
				} else {
					$images_front = $images_front[0]->name;
				}
				$list .= ' <li>
                                <a href="?product=' . $item->id . '" class="item-link">
                                        <img src="' . App::BasePath() . '/images/' . $images_front . '" alt="" />
                                        <div class="entry-label">
                                            <h4 class="car-title">' . $item->name . ' ' . $item->name_model . '</h4>
                                            <span class="price-tag">' . $item->price . ' ' . $item->currency . '</span>
                                        </div>
                                    </a>
                                </li>';
			}
		} else if ($type == "laptop") {
			
		} else if ($type == "monitor") {
			
		} else if ($type == "components") {
			
		} else {
			
		}
		$models = AppSql::findAll("*", "`prd_model`", array(":enable" => "1"), "", "DESC");

		$view = new View("list_product", array("list" => $list, "model" => $models));
		return $view->render();
	}

}

?>