-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 18, 2016 at 06:09 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cms_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `constants`
--

CREATE TABLE `constants` (
  `id` int(11) NOT NULL,
  `type` varchar(255) NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `constants`
--

INSERT INTO `constants` (`id`, `type`, `value`) VALUES
(1, 'coin', '{"Currency":"EUR","Currency":"RON"}');

-- --------------------------------------------------------

--
-- Table structure for table `prd_image`
--

CREATE TABLE `prd_image` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_model`
--

CREATE TABLE `prd_model` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `create_by` int(11) NOT NULL DEFAULT '0',
  `create_at` date NOT NULL,
  `modified_by` int(11) NOT NULL DEFAULT '0',
  `modified_at` date NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_product`
--

CREATE TABLE `prd_product` (
  `id` int(11) NOT NULL,
  `id_model` int(11) NOT NULL DEFAULT '0',
  `id_template` int(11) DEFAULT NULL,
  `name` text NOT NULL,
  `price` double NOT NULL DEFAULT '0',
  `currency` varchar(10) NOT NULL DEFAULT 'EUR',
  `quantity` int(11) NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `prd_template`
--

CREATE TABLE `prd_template` (
  `id` int(11) NOT NULL,
  `description` text NOT NULL,
  `create_by` int(11) NOT NULL,
  `create_at` date NOT NULL,
  `modified_by` int(11) NOT NULL,
  `modified_at` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_constant`
--

CREATE TABLE `tbl_constant` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `value` varchar(200) NOT NULL,
  `status` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_curier`
--

CREATE TABLE `tbl_curier` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `employees` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_curier_employee`
--

CREATE TABLE `tbl_curier_employee` (
  `id` int(11) NOT NULL,
  `id_curier` int(11) NOT NULL,
  `fist_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_permisions`
--

CREATE TABLE `tbl_permisions` (
  `id` int(11) NOT NULL,
  `permisions` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_profile`
--

CREATE TABLE `tbl_profile` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `country` varchar(50) NOT NULL,
  `genere` varchar(30) NOT NULL,
  `region` varchar(30) NOT NULL,
  `street` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_statistics`
--

CREATE TABLE `tbl_statistics` (
  `id` int(11) NOT NULL,
  `firefox` int(11) NOT NULL,
  `chrome` int(11) NOT NULL,
  `iexplore` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `ip_address` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subscribe`
--

CREATE TABLE `tbl_subscribe` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `create_at` date NOT NULL,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `id_profile` int(11) DEFAULT NULL,
  `id_permision` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `superadmin` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `id_profile`, `id_permision`, `username`, `password`, `superadmin`, `status`) VALUES
(1, NULL, 1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `constants`
--
ALTER TABLE `constants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_image`
--
ALTER TABLE `prd_image`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_model`
--
ALTER TABLE `prd_model`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `prd_product`
--
ALTER TABLE `prd_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_model` (`id_model`),
  ADD KEY `id_template` (`id_template`);

--
-- Indexes for table `prd_template`
--
ALTER TABLE `prd_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_constant`
--
ALTER TABLE `tbl_constant`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_curier`
--
ALTER TABLE `tbl_curier`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_curier_employee`
--
ALTER TABLE `tbl_curier_employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_curier` (`id_curier`);

--
-- Indexes for table `tbl_permisions`
--
ALTER TABLE `tbl_permisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_subscribe`
--
ALTER TABLE `tbl_subscribe`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_permision` (`id_permision`),
  ADD KEY `id_profile` (`id_profile`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `constants`
--
ALTER TABLE `constants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `prd_image`
--
ALTER TABLE `prd_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prd_model`
--
ALTER TABLE `prd_model`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prd_product`
--
ALTER TABLE `prd_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `prd_template`
--
ALTER TABLE `prd_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_constant`
--
ALTER TABLE `tbl_constant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_curier`
--
ALTER TABLE `tbl_curier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_curier_employee`
--
ALTER TABLE `tbl_curier_employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_permisions`
--
ALTER TABLE `tbl_permisions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_statistics`
--
ALTER TABLE `tbl_statistics`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_subscribe`
--
ALTER TABLE `tbl_subscribe`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `prd_product`
--
ALTER TABLE `prd_product`
  ADD CONSTRAINT `fk_model` FOREIGN KEY (`id_model`) REFERENCES `prd_model` (`id`),
  ADD CONSTRAINT `fk_template` FOREIGN KEY (`id_template`) REFERENCES `prd_template` (`id`);

--
-- Constraints for table `tbl_curier`
--
ALTER TABLE `tbl_curier`
  ADD CONSTRAINT `fk_curier` FOREIGN KEY (`id`) REFERENCES `tbl_curier_employee` (`id_curier`);

--
-- Constraints for table `tbl_permisions`
--
ALTER TABLE `tbl_permisions`
  ADD CONSTRAINT `fk_permisions` FOREIGN KEY (`id`) REFERENCES `tbl_user` (`id_permision`);

--
-- Constraints for table `tbl_profile`
--
ALTER TABLE `tbl_profile`
  ADD CONSTRAINT `fk_users` FOREIGN KEY (`id`) REFERENCES `tbl_user` (`id_profile`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
