/*
	 * Fonction qui permet de céer un objet avec les fichiers 
	 * charges dans le form de 'upload'
	 */
	protected function createFiles($data) {
		foreach($data as $file)
			$files[] = new File(NULL, $file['name'], $file['filename']);
		
		return isset($files) ? $files : [];
	}
	
	/*
	 * Fonction qui charge automatiquement les fichiers dans le CRM
	 * Extensions acceptées : images, pdf, excel, word, power point
	 * Retourne des objets File spécifiques aux nouveaux emplacements 
	 * dans le CRM (files [temp/documents/images])
	 */
	public function uploadFiles($data) {
		return $this->getDao()->uploadFiles($data);
	}
	
	/*
	 * Lit tous les fichiers d'une maison 
	 */
	public function readFiles($data) {
		return $this->getDao()->readFiles($data);
	}
	
	/*
	 * Enleve un fichier de la 'dropzone' spécifique à cette maison
	 */
	public function removeFile($data) {
		return $this->getDao()->removeFile($data);
	}