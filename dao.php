protected function addFiles($maison) {
		if($maison->getFiles() !== NULL) {
			$this->uploadFiles(
				array(
					'select_id' => $maison->getIdMaison(),
					'select_etat' => $maison->getEtat()->getId(),
					'files' => $maison->getFiles()
				)
			);
		}
	}
	
	public function uploadFiles($data) {
		if(isset($data['maison_id'])) {
			$upFile =  new FileUpload($data['maison_id'], new Etat($data['etat_maison']), $_FILES);
			if($upFile->getIdMaison() !== NULL)
				$this->getDaoFileUpload()->add($upFile);
		}else {
			$upFile =  new FileUpload($data['select_id'], new Etat($data['select_etat']), $data['files']);
			$this->getDaoFileUpload()->add($upFile);
		}
		return $upFile;
	}
	
	public function readFiles($data) {
		if(isset($data['maison_id'])) {
			return $this->getDaoFileUpload()->readList(
				new FileUpload($data['maison_id'], new Etat($data['etat_maison']))
			);
		}
		return [];
	}
	
	public function removeFile($data) {
		if(isset($data['maison_id']) && $data['maison_id'] !== '') {
			$upFile = new FileUpload($data['maison_id'], new Etat($data['etat_maison']));
			$upFile->addFile(new File(NULL, NULL, $data['file_name'], 'no'));

			return $this->getDaoFileUpload()->delete($upFile);
		} else {
			(new File(NULL, NULL, $data['file_name'], 'yes'))->delete();
			return [];
		}
	}