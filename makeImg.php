<?php

/*
 * Description of FileUpload
 * 
 * C'est un emballage pour un ou plusieurs fichiers qui nécessitent
 * un travail préalable afin de les héberger dans le dossier 'files'
 * 
 * IM
 */

namespace DubInfo_gestion_immobilier\model;

use DubInfo_gestion_immobilier\model\exceptions\StringAttributeTooLong;
use DubInfo_gestion_immobilier\model\exceptions\BadTypeException;


class FileUpload implements \JsonSerializable{
	const THUMBNAIL_WIDTH = 128;
	const THUMBNAIL_HEIGHT = 128;
	
	const ARRAY_EXT = array('jpeg', 'jpg', 'png', 'gif', 'pdf', 'doc', 'docx', 'xls', 'xlsx', 'ppt', 'pptx');
    /**
     * @var integer 
     */
    private $_id_maison;
    
    /**
     * @var integer 
     */
    private $_etat;
	
	/**
     * @var Object File 
     */
    private $_files;
    
    /**
     * @param type $id
     * @param type $nom
     * @param type $prenom
     * @param type $login
     * @param type $password
     * @throws BadTypeException
     * @throws StringAttributeTooLong
     */
    public function __construct($maison_id = NULL, $etat_maison = NULL, $filesPHP = NULL) {
		$this->setIdMaison($maison_id);
		$this->setEtat($etat_maison);
		$this->setFiles(is_array($filesPHP)?$filesPHP:NULL);
    }
    
    /**
     * 
     * @return int
     */
    public function getIdMaison() {
        return $this->_id_maison;
    }
    
    /**
     * 
     * @param int $id_maison
     * @throws BadTypeException
     */
    public function setIdMaison($id_maison) {
        $this->_id_maison = CheckTyper::isInteger($id_maison, 'id maison', __CLASS__);
    }
   
	
	/**
     * 
     * @return Etat
     */
    public function getEtat() {
        return $this->_etat;
    }
    
    /**
     * 
     * @param Etat $etat
     * @throws BadTypeException
     */
    public function setEtat($etat) {
        $this->_etat = CheckTyper::isModel($etat, Etat::class, 'etat', __CLASS__);
    }
	
	/**
     * 
     * @return File
     */
    public function getFile($id) {
        return $this->_files[$id];
    }
	
	/**
     * 
     * @return File
     */
    public function getFiles() {
        return $this->_files;
    }
    
    /**
     * 
	 * Distribue les fichiers dans les répertoires requis
	 * 
	 * S'il s'agit d'un 'chargement':
	 *  - (pouvoir Ajouter)Si la maison n'existe pas:
	 *		- les fichiers sont stockés temporairement
	 *	- (pouvoir Editer)Si la maison existe déjà:
	 *		- les fichiers sont chargés dans les répertoirs indiqués 
	 * 
     * @param File $files
     * @throws BadTypeException
     */
    public function setFiles($files) {
		$_files = array();
	
		if(is_array($files)) {
			foreach($files as $file) {
				if(is_a($file, File::class)) {
					$filename = $file->getName();
					$filepath = $file->getFileName();
					$fileext = strtolower(pathinfo($filepath, PATHINFO_EXTENSION));
				
					if(!in_array($fileext, self::ARRAY_EXT)) continue;
					
					$tmp_file = HOUSE_TMPFILES_PATH.$filepath;
					$mid = $this->getIdMaison();
					$eid = $this->getEtat()->getId();
					if($fileext === 'jpeg' || $fileext === 'jpg' || $fileext === 'gif' || $fileext === 'png') {
						$tmp_file_name = $mid."_".$eid."_".$filepath;
						$tmp_file_path = HOUSE_IMAGES_PATH.$tmp_file_name;
						if(rename($tmp_file, $tmp_file_path)) {
							array_push($_files, new File(NULL, $filename, $tmp_file_name, 'no'));

							$tmp_thumbnail_path = HOUSE_THUMBNAILS_PATH.$tmp_file_name;
							$this->make_thumbnail_from_file($tmp_file_path, $tmp_thumbnail_path, $fileext);
						}
					}else {
						$tmp_file_name = $mid."_".$eid."_".$filepath;
						$tmp_file_path = HOUSE_DOCUMENTS_PATH.$tmp_file_name;
						if(rename($tmp_file, $tmp_file_path)) {
							array_push($_files, new File(NULL, $filename, $tmp_file_name, 'no'));
						}
					}
				}else {
					if(isset($file['error']) && $file['error'] === 0) {
						$filename = $file['name'];
						$filepath = $file['tmp_name'];
						$filehash = hash_file('sha1', $filepath);
						$fileext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

						if(!in_array($fileext, self::ARRAY_EXT)) continue;

						if($this->getIdMaison() === NULL) {
							$tmp_file_path = HOUSE_TMPFILES_PATH.$filehash.'.'.$fileext;
							if(move_uploaded_file($filepath, $tmp_file_path)) {
								array_push($_files, new File(NULL, $filename, $filehash.'.'.$fileext, 'yes'));
							}
						}else {
							$mid = $this->getIdMaison();
							$eid = $this->getEtat()->getId();
							if($fileext === 'jpeg' || $fileext === 'jpg' || $fileext === 'gif' || $fileext === 'png') {
								$tmp_file_name = $mid.'_'.$eid.'_'.$filehash.'.'.$fileext;
								$tmp_file_path = HOUSE_IMAGES_PATH.$tmp_file_name;
								if(move_uploaded_file($filepath, $tmp_file_path)) {
									array_push($_files, new File(NULL, $filename, $tmp_file_name, 'no'));

									$tmp_thumbnail_path = HOUSE_THUMBNAILS_PATH.$tmp_file_name;
									$this->make_thumbnail_from_file($tmp_file_path, $tmp_thumbnail_path, $fileext);
								}
							}else {
								$tmp_file_name = $mid.'_'.$eid.'_'.$filehash.'.'.$fileext;
								$tmp_file_path = HOUSE_DOCUMENTS_PATH.$tmp_file_name;
								if(move_uploaded_file($filepath, $tmp_file_path)) {
									array_push($_files, new File(NULL, $filename, $tmp_file_name, 'no'));
								}
							}
						}
					}
				}
			}
		}
		
		$this->_files = CheckTyper::isArrayOfModel($_files, File::class, 'files', __CLASS__);
    }
    
	/*
	 * Rajoute un fichier dans la liste du 'wrapper'
	 * 
	 * @param File $file
	 */
	
	public function addFile($file) {
		$this->_files[] = CheckTyper::isModel($file, File::class, 'add file', __CLASS__);
	}
	
	/*
	 * Convertit une image de toute mesure dans un thumbnail
	 * de mesure THUMBNAIL_WIDTH x THUMBNAIL_HEIGHT
	 * 
	 * @param string $source
	 * @param string $destination
	 * @param string $ext
	 */
	
	protected function make_thumbnail_from_file($source, $destination, $ext) {
		list($w_orig, $h_orig) = getimagesize($source);
		
		$img = NULL;
		if ($ext == "gif"){ 
			$img = imagecreatefromgif($source);
		} else if($ext =="png"){ 
			$img = imagecreatefrompng($source);
		} else { 
			$img = imagecreatefromjpeg($source);
		}
		
		$w = self::THUMBNAIL_WIDTH;
		$h = self::THUMBNAIL_HEIGHT;
		
		$ratio_img = $w_orig / $h_orig;
		$ratio_des = $w / $h; 
		
		if($ratio_img > $ratio_des) {
			$desired_height = $h;
			$desired_width = floor( ($w_orig * $desired_height) / $h_orig);
		}else {
			$desired_width = $w;
			$desired_height = floor( ($h_orig * $desired_width) / $w_orig);
		}
		
		$src_x = ($desired_width - $w) / 2;
		$src_y = ($desired_height - $h) / 2;
		
		$tci = imagecreatetruecolor($desired_width, $desired_height);
		$tci2 = imagecreatetruecolor($w, $h);
		if ($ext == "gif"){ 
			$background = imagecolorallocate($tci, 0, 0, 0);
			$background2 = imagecolorallocate($tci2, 0, 0, 0);
			// removing the black from the placeholder
			imagecolortransparent($tci, $background);
			imagecolortransparent($tci2, $background2);
		} else if($ext =="png"){ 
			$background = imagecolorallocate($tci, 0, 0, 0);
			$background2 = imagecolorallocate($tci2, 0, 0, 0);
			// removing the black from the placeholder
			imagecolortransparent($tci, $background);
			imagecolortransparent($tci2, $background2);

			// turning off alpha blending (to ensure alpha channel information 
			// is preserved, rather than removed (blending with the rest of the 
			// image in the form of black))
			imagealphablending($tci, false);
			imagealphablending($tci2, false);

			// turning on alpha channel information saving (to ensure the full range 
			// of transparency is preserved)
			imagesavealpha($tci, true);
			imagesavealpha($tci2, true);
		}
		imagecopyresampled($tci, $img, 0, 0, 0, 0, $desired_width, $desired_height, $w_orig, $h_orig);
		imagecopy($tci2, $tci, 0, 0, $src_x, $src_y, $w, $h);

		if ($ext == "gif"){ 
			imagegif($tci2, $destination);
		} else if($ext =="png"){ 
			imagepng($tci2, $destination);
		} else { 
			imagejpeg($tci2, $destination, 84);
		}
	}
	
    public function jsonSerialize() {
        return [
			'maison_id' => $this->getIdMaison(),
			'etat_maison' => $this->getEtat()->getId(),
			'files' => $this->getFiles()
		];
    }
}
